<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegistrarClienteRequest;
use App\Http\Requests\IniciarSesionRequest;
use App\Http\Requests\RegistrarEntrenadorRequest;
use App\Http\Requests\LoginRequest;
use Illuminate\Database\QueryException;
use App\Http\Controllers\RutinaController;
use App\Models\Usuario;
use App\Models\Entrenador;
use App\Models\Rutina;
use App\Models\Rutinasxusuario;
use App\Services\UploadFileService;
use App\Exceptions\UploadFileException;

class AdminController extends Controller
{
    // Declaración de vaiables
    public $clienteModel;
    public $entrenadorModel;
    public $rutinaModel;
    public $rutinaxusuarioModel;
    private $uploadService;


    public function __construct() {
        $this->usuarioModel = new Usuario();
        $this->entrenadorModel = new Entrenador();
        $this->rutinaModel = new Rutina();
        $this->rutinaxusuarioModel = new Rutinasxusuario();
    }

    /**
     * Mostrar vista registroCliente
     */
    public function mostrarRegistroCliente() {
        return view("cliente.registroCliente");
    }

    /**
     * Mostrar vista registroEntrenador
     */
    public function mostrarRegistroEntrenador() {
        return view('entrenador.registroEntrenador');
    }


    /**
     * Crear un nuevo usuario cliente
     */
    public function crearUsuarioCliente(RegistrarClienteRequest $request, UploadFileService $UploadFileService) {
        $request->flash();
        $success = null;
        try {
            // Guardamos la foto
            $file = $request->file('fotoUsuario');
            $this->uploadService = $UploadFileService;
            $this->uploadService->uploadFile($file);
            // Nombre
            $nombre = $request->input('nombre');
            // Apellidos
            $apellidos = $request->input('apellidos');
            // Descripcion
            $descripcion = $request->input('descripcion');
            // Email
            $email = $request->input('email');
            // Imagen
            $fotoUsuario = $request->fotoUsuario->getClientOriginalName();
            // Contraseña
            $password = $request->input('password');
            // Tipo usuario
            $tipo = $request->input('tipo');
            $success = $this->usuarioModel->saveCliente($nombre, $apellidos, $descripcion, $email, $fotoUsuario, $password, $tipo);
        } catch (QueryException $exception) {
            return back()->withError("Usuario con email ".$email." ya existe")->withInput();
        } catch (UploadFileException $exception) {
            return back()->withError($exception->message())->withInput();
        }
        return redirect()->action([AdminController::class, 'mostrarRegistroCliente'], ['success' => $success]);
    }

    /**
     * Crear un nuevo usuario entrenador
     */
    public function crearUsuarioEntrenador(RegistrarEntrenadorRequest $request, UploadFileService $UploadFileService) {
        $request->flash();
        $success = null;
        try {
            // Guardamos la foto
            $file = $request->file('fotoUsuario');
            $this->uploadService = $UploadFileService;
            $this->uploadService->uploadFile($file);
            // Nombre
            $nombre = $request->input('nombre');
            // Apellidos
            $apellidos = $request->input('apellidos');
            // Dni
            $dni = $request->input('dni');
            // Telefono
            $telefono = $request->input('telefono');
            // Descripcion
            $descripcion = $request->input('descripcion');
            // Email
            $email = $request->input('email');
            // Imagen
            $fotoUsuario = $request->fotoUsuario->getClientOriginalName();
            // Contraseña
            $password = $request->input('password');
            // Tipo usuario
            $tipo = $request->input('tipo');
            $success = $this->usuarioModel->saveEntrenador($nombre, $apellidos, $dni, $telefono, $descripcion, $email, $fotoUsuario, $password, $tipo);
        } catch (QueryException $exception) {
            return back()->withError("Usuario con email ".$email." ya existe")->withInput();
        } catch (UploadFileException $exception) {
            return back()->withError($exception->message())->withInput();
        }
        return redirect()->action([AdminController::class, 'mostrarRegistroEntrenador'], ['success' => $success]);
    }

    /**
     * Mostrar login
     */
    public function showLogin() {
        return view("login");
    }
    /**
     * Mostrar pagina inicio de cliente
     */
    public function showInicioCliente() {
        return view("cliente.inicioCliente");
    }

    /**
     * Mostrar pagina inicio de entrenador
     */
    public function showInicioEntrenador(Request $request) {
        // Guardamos el id del entrenador
        $idEntrenador = $request->session()->get('usuario')['id'];
        // Guardamos todas las rutinas que se deben mostrar
        $rutinasEntrenador = $this->rutinaModel->misRutinasEntrenador($idEntrenador);
        $suscripcionRutina = [];
        foreach ($rutinasEntrenador as $rutina) {
            $suscripcion = $this->rutinaxusuarioModel->getRutinaxusuarioByID($rutina->idRutina);
            array_push($suscripcionRutina, count($suscripcion));
        }
        $numUsers = $this->usuarioModel->countUsers();
        // return $rutinasEntrenador;
        return view("entrenador.misRutinasEntrenador", ['rutinasEntrenador' => $rutinasEntrenador, 'suscripcionRutina' => $suscripcionRutina, 'countUsers' => $numUsers]);
    }

    /**
     * Loguear un usuario
     */
    public function loginUser(LoginRequest $request) {
        // Guardamos los campos del formulario
        // Email
        $email = $request->input("email");
        // Password
        $password = $request->input("password");
        // Comprobamos si esta registrado
        $registreTrobat = $this->usuarioModel->checkUser($email, $password);
        $registreTrobatAux = json_decode($registreTrobat, true);
        if (empty($registreTrobatAux)) {
            return back()->withError("El usuario introducido no existe")->withInput();
        } else {
            if ($registreTrobat[0]->tipo == "entrenador") {
                $request->session()->put('tipoUsuario', "entrenador");
                $request->session()->put('usuario', ['id' => $registreTrobat[0]->idUsuario, 'nombre' => $registreTrobat[0]->nombre, 'apellidos' => $registreTrobat[0]->apellidos, 'foto' => $registreTrobat[0]->fotoUsuario]);
                return redirect()->action([AdminController::class, 'showInicioEntrenador']);
            } else {
                $request->session()->put('tipoUsuario', 'cliente');
                $request->session()->put('usuario', ['id' => $registreTrobat[0]->idUsuario, 'nombre' => $registreTrobat[0]->nombre, 'apellidos' => $registreTrobat[0]->apellidos, 'foto' => $registreTrobat[0]->fotoUsuario]);
                return redirect()->action([RutinaController::class, 'mostrarRutinas']);
            }
        }
    }

    public function loginForm() {
        return view('login');
    }

    public function inicio() {
        return view('inicioCliente');
    }

    /**
     * logout
     * 
     * Cerrar sesión del usuario logueado
     */
    public function logoutUser(Request $request) {
        // Cerramos las variables de sesion
        $request->session()->flush();
        // Redirijimos a la pagina principal
        return redirect('/');
    }

    /**
     * countUsers
     * 
     * Contar usuarios cliente registrados
     */
    public function countUsers() {
        
        return view('');
    }

    
}
