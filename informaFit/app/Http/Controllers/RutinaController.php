<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CrearRutinaRequest;
use App\Models\Rutina;
use App\Models\Ejercicio;
use App\Models\DetalleRutina;
use App\Models\Rutinasxusuario;
use App\Services\UploadFileService;
use App\Exceptions\UploadFileException;
use App\Exceptions\ErrorException;
use App\Http\Controllers\UsuarioController;
use App\Http\Requests\RequestAjustes;

class RutinaController extends Controller
{
    // Declaración de variables
    public $rutinaModel;
    public $rutinas;
    public $ejercicios;
    public $dias;
    private $uploadService;

    public function __construct() {
        $this->rutinaModel = new Rutina();
        $this->ejercicioModel = new Ejercicio();
        $this->detalleRutinaModel = new DetalleRutina();
        $this->rutinaxusuarioModel = new Rutinasxusuario();
    }

    // Método para mostrar todas las rutinas
    public function mostrarRutinas(Request $request) {
        // Miramos si el usuario está registrado o no
        $tipoUsuario = $request->session()->get('tipoUsuario');
        if ($tipoUsuario == 'cliente') {
            return view("cliente.inicioCliente")->with(['rutinas' => $this->rutinaModel->rutinaEntrenador()]);
        }
        return view("rutinasGuest")->with(['rutinas' => $this->rutinaModel->rutinaEntrenador()]);
    }

    public function mostrarEjercicios() {
        // return view("cliente.inicioCliente")->with(['rutinas' => $this->rutinaModel->get()]);
        return view("entrenador.crearRutina")->with(['ejercicios' => $this->ejercicioModel->get()]);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////FILTRO CLIENTE////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public function mostrarPalabraClave(Request $request) {
        // Guardamos la palabra clave enviada por el usuario
        $keyword = $request->input('keyword');
        // Miramos si el usuario está registrado o no
        $tipoUsuario = $request->session()->get('tipoUsuario');
        if ($tipoUsuario == 'cliente') {
            return view("cliente.inicioCliente")->with(['rutinas' => $this->rutinaModel->palabraClave($keyword)]);
        }
        return view('rutinasGuest')->with(['rutinas' => $this->rutinaModel->palabraClave($keyword)]);
    }


    public function mostrarRutinaFiltros(Request $request) {
        //dificultad
        $dificultad = $request->input("dificultad");
        //duracion
        $duracion = $request->input("duracion");
        //zonaTrabajo
        $zonaTrabajo = $request->input("zonaTrabajo");
        //objetivo
        $objetivo = $request->input("objetivo");

        //Creamos la query
        $rutinas = $this->rutinaModel->query();

        //Concatenamos las querys
        if(!empty($dificultad)) {
            $rutinas->dificultad($dificultad);
        }
        if(!empty($duracion)) {
            $rutinas->duracion($duracion);
        }
        if(!empty($zonaTrabajo)) {
            $rutinas->zonaTrabajo($zonaTrabajo);
        }
        if(!empty($objetivo)) {
            $rutinas->objetivo($objetivo);
        }

        // Miramos si el usuario está registrado o no
        $tipoUsuario = $request->session()->get('tipoUsuario');
        if ($tipoUsuario == 'cliente') {
            return view("cliente.inicioCliente")->with(['rutinas' => $rutinas
                ->join('usuarios', 'rutinas.idUsuario', '=', 'usuarios.idUsuario')
                ->get(['rutinas.*', 'rutinas.descripcion as rutinaDesc', 'usuarios.*', 'usuarios.nombre as usuarioNom'])]);
        }
        return view("rutinasGuest")->with(['rutinas' => $rutinas
            ->join('usuarios', 'rutinas.idUsuario', '=', 'usuarios.idUsuario')
            ->get(['rutinas.*', 'rutinas.descripcion as rutinaDesc', 'usuarios.*', 'usuarios.nombre as usuarioNom'])]);
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////FILTRO ENTRENADOR/////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function mostrarPalabraClaveEntrenador(Request $request) {
        // Guardamos la palabra clave enviada por el usuario
        $keyword = $request->input('keyword');
        // Miramos si el usuario está registrado o no
        return view("entrenador.misRutinasEntrenador")->with(['rutinasEntrenador' => $this->rutinaModel->palabraClave($keyword)]);
    }

    public function ordenarRutinaFiltros(Request $request) {
        // //dificultad
        // $dificultad = $request->input("dificultad");
        // //duracion
        // $fechaPublicacion = $request->input("fechaPublicacion");
        $orden = $request->input("orden");
        //Creamos la query
        $rutinasEntrenador = $this->rutinaModel->query();
        $idEntrenador = $request->session()->get('usuario')['id'];
        $rutinasEntrenador->misRutinasEntrenador($idEntrenador);
        $rutinasEntrenador->order($orden);
        // return $rutinasEntrenador->get('rutinas.*', 'rutinas.created_at as fecha_creacion');
        // return $rutinasEntrenador->get();
        // $rutinasEntrenador->orderDificultad($dificultad);
        // $rutinasEntrenador->orderFechaCreacion($fechaPublicacion);
        return view("entrenador.misRutinasEntrenador")->with(['rutinasEntrenador' => $rutinasEntrenador->get('rutinas.*', 'rutinas.created_at as fecha_creacion')]);

    }

    /**
     * ordenarSuscripciones
     *
     * Ordenar suscripciones
     */
    public function ordenarSuscripciones(Request $request) {
        $orden = $request->input("orden");
        //Creamos la query
        $suscripciones = $this->rutinaModel->query();
        // Id del cliente
        $idCliente = $request->session()->get('usuario')['id'];
        // Recogemos las rutinas de ese cliente
        $suscripciones->misRutinasCliente($idCliente);
        // Las ordenamos
        $suscripciones->order($orden);
        return view("cliente.misRutinasCliente")->with(['rutinas' => $suscripciones->get(['usuarios.*', 'entrenador.nombre as nombreEntrenador', 'entrenador.apellidos as apellidosEntrenador', 'rutinas.*', 'rutinas.descripcion as rutinaDesc', 'rutinasxusuarios.*', 'rutinas.created_at as rutinaFecha'])]);
    }


    // Método para mostrar una sola rutina
    public function descripcionRutina(Request $request) {
        // Guardamos el id de la rutina
        $idRutina = $request->input("idRutina");

        // Llamamos al método del modelo y guardamos el resultado
        $rutinas = $this->rutinaModel->descripcionRutina($idRutina)->get();
        $dias = $this->rutinaModel->dias($idRutina)->get();
        $ejercicios = $this->rutinaModel->ejercicios($idRutina)->get();
        $tipoUsuario = $request->session()->get('tipoUsuario');
        // Obtenemos el id de las rutinas en las que el usuario está suscrito
        $idUsuario = $request->session()->get('usuario')['id'];
        $rutinasSuscritas = $this->rutinaModel->misRutinasCliente($idUsuario);

        $idRutinasSuscritas = [];
        for($i = 0; $i < count($rutinasSuscritas); $i++) {
            $idRutinasSuscritas[$i] = $rutinasSuscritas[$i]->idRutina;
        }

        // Devolvemos la vista
        if ($tipoUsuario == 'cliente') {
            return view('cliente.descripcionRutina', compact('rutinas', 'dias', 'ejercicios', 'idRutinasSuscritas', 'rutinasSuscritas'));
        } else {
            return view('entrenador.descripcionRutina', compact('rutinas', 'dias', 'ejercicios'));
        }
    }

    public function crearRutina(CrearRutinaRequest $request, UploadFileService $UploadFileService) {
        //Guardamos los datos para crear la rutina
        $titulo = $request->input("title");
        $description = $request->input("description");
        $duration = $request->input("duration");
        $difficulty = $request->input("difficulty");
        $material = $request->input("material");
        $zone = $request->input("zone");
        $objective = $request->input("objective");
        $link = $request->input("link");
        $diet = $request->file("diet");
        $nameExercise = $request->input("nameExercise");
        $descripcionEjercicio = $request->input("descripcionEjercicio");
        $repeticiones = $request->input("repeticiones");
        //$foto = $request->input("foto");
        $foto = $request->file("foto");
        $dias = $request->input("dias");

        // Importamos el service
        $this->uploadService = $UploadFileService;
        // Convertimos el link de youtube a embed
        if (empty($link)) {
            $linkEmbed = null;
        } else {
            $linkEmbed = str_replace("watch?v=", "embed/", $link);
        }
        if (empty($diet)) {
            $nameDiet = null;
        } else {
            // Guardar pdf
        $this->uploadService->uploadPdf($diet);
        $nameDiet = $diet->getClientOriginalName();
        }
        //save rutina
        $this->rutinaModel->saveRutina($request->session()->get('usuario')['id'], $titulo, $description, $duration, $linkEmbed, $difficulty, $objective, $material, $zone, $nameDiet);
        $lastIdRutina = $this->rutinaModel->lastIdRutina();
        //save ejercicio
        for ($i = 0; $i < count($nameExercise); $i++) {
            try {
                if (!empty($foto[$i])) {
                    $this->uploadService->uploadExerciseImg($foto[$i]);
                    $fotoNombre = $request->foto[$i]->getClientOriginalName();
                    $this->ejercicioModel->saveEjercicio($nameExercise[$i], $descripcionEjercicio[$i], $fotoNombre);
                } else {
                    return back()->withError("Introduce una foto de ejercicio")->withInput();
                }
            } catch (UploadFileException $exception) {
                return back()->withError($exception->message())->withInput();
            }
            $lastIdExercise = $this->ejercicioModel->lastIdEjercicio();
            $eachDay = explode(',', $dias[$i]);
            //save detalleRutina
            for($z = 0; $z < count($eachDay); $z++) {
                $especificDay = $eachDay[$z];
                $this->detalleRutinaModel->saveDetalleRutina($lastIdRutina->idRutina, $lastIdExercise->idEjercicio, $especificDay, $repeticiones[$i]);
            }
        }

        return redirect('entrenador/descripcionRutina?idRutina='.$lastIdRutina->idRutina);
    }

    /**
     * misRutinasClientePalabra
     *
     * Mostrar las rutinas de un cliente cuando busca
     * por palabra clave
     */
    public function misRutinasClientePalabra(Request $request) {
        // Guardamos la palabra que ha escrito el usuario
        $keyword = $request->input('keyword');
        $idCliente = $request->session()->get('usuario')['id'];
        // Guardamos las rutinas
        $rutinas = $this->rutinaModel->misRutinasClientePalabra($idCliente, $keyword);
        // Devolvemos las rutinas
        return view('cliente.misRutinasCliente')->with(['rutinasCliente' => $rutinas]);
    }

    /**
     * misRutinasClienteOrdenar
     *
     * Mostrar las rutinas de un cliente ordenadas
     * por el campo que elija
     */
    public function misRutinasClienteOrdenar(Request $request) {
        // Guardamos el campo por el que quiere ordenar
        $ordenarPor = $request->input("ordenar");
        $idCliente = $request->session()->get('usuario')['id'];
        // Guardamos las rutinas
        $rutinas = $this->rutinaModel->misRutinasClienteOrdenar($idCliente, $ordenarPor);
        // Devolvemos las rutinas
        return view('cliente.misRutinasCliente')->with(['rutinasCliente' => $rutinas]);
    }

    /**
     * eliminarRutina
     *
     * Eliminar rutina
     */
    public function eliminarRutinaEntrenador(Request $request) {
        // Guardamos el id de la rutina
        $idRutina = $request->input("idRutina");
        // Obtenemos la rutina con ese id
        $rutina = Rutina::find($idRutina);
        $success = $rutina->delete();
        return redirect()->action([AdminController::class, 'showInicioEntrenador'], ['success' => $success]);
    }

    /**
     * eliminarRutinas
     *
     * Eliminar todas las rutinas
     */
    public function eliminarRutinasEntrenador(Request $request) {
        // Obtenemos el id del entrenador
        $idUsuario = $request->session()->get('usuario')['id'];
        $rutinas = $this->rutinaModel->misRutinasEntrenador($idUsuario);
        foreach ($rutinas as $rutina) {
            // // Id rutina
            $rutinaId = $rutina->idRutina;
            // Borramos rutina
            $rutinaObj = Rutina::find($rutinaId);
            $success = $rutinaObj->delete();
        }
        return redirect()->action([AdminController::class, 'showInicioEntrenador'], ['successDelete' => $success]);
    }

    /**
     * eliminarSuscripcion
     *
     * Eliminar suscripcion de cliente
     */
    public function eliminarSuscripcion(Request $request) {
        // Guardamos el id de la rutina
        $idRutina = $request->input("idRutina");
        // Guardamos el id del cliente
        $idCliente = $request->session()->get('usuario')['id'];
        $suscripcion = $this->rutinaxusuarioModel->getSuscripcionCliente($idCliente, $idRutina)->get();
        // Obtenemos la rutina con ese id
        $rutina = Rutinasxusuario::find($suscripcion[0]->idRutinaxusuario);
        $success = $rutina->delete();
        return redirect()->action([UsuarioController::class, 'misRutinasCliente'], ['success' => $success]);
    }

    /**
     * eliminarSuscripciones
     *
     * Eliminar todas las suscripciones
     */
    public function eliminarSuscripciones(Request $request) {
        // Obtenemos el id del cliente
        $idUsuario = $request->session()->get('usuario')['id'];
        // Rutinas suscriptas por el usuario
        $suscripciones = $this->rutinaxusuarioModel->getSuscripcionesCliente($idUsuario)->get();
        foreach ($suscripciones as $suscripcion) {
            // Id suscripcion
            $suscripcionId = $suscripcion->idRutinaxusuario;
            // Borramos rutina
            $suscripcionObj = Rutinasxusuario::find($suscripcionId);
            $success = $suscripcionObj->delete();
        }
        return redirect()->action([UsuarioController::class, 'misRutinasCliente'], ['successDelete' => $success]);
    }

    /**
     * showModificarRutina
     *
     * Mostrar la vista para modificar una rutina en concreto de un entrenador
     */
    public function showModificarRutina(Request $request) {
        // Guardamos el id de la rutina
        $idRutina = $request->input("idRutina");
        $rutina = $this->rutinaModel->descripcionRutina($idRutina)->get();
        // $dias = $this->rutinaModel->dias($idRutina)->get();
        $ejercicios = $this->rutinaModel->ejerciciosDistinct($idRutina)->get();
        $dias = $this->rutinaModel->diasRutinaByEjercicio($idRutina)->get();
        $totalEjercicios = $this->ejercicioModel->countEjercicios();

        $linkEmbed  = "";
        if(isset($rutina[0]->link_video)){
        $linkEmbed = $rutina[0]->link_video;
        }
        $link = str_replace("embed/", "watch?v=", $linkEmbed);
        return view('entrenador.modificarRutina', ['rutina' => $rutina, 'ejercicios' => $ejercicios, 'link' => $link, 'dias' => $dias, 'totalEjercicios' => $totalEjercicios]);
    }

    /**
     * modificarRutina
     *
     * Modificar una rutina en concreto de un entrenador
     */
    public function modificarRutina(Request $request, UploadFileService $UploadFileService) {
        // Importamos el service
        $this->uploadService = $UploadFileService;
        // Obtenemos todos los inputs que ha puesto el entrenador
        $titulo = $request->input("title");
        $descripcion = $request->input("description");
        $duracion = $request->input("duration");
        $dificultad = $request->input("difficulty");
        $materiales = $request->input("material");
        $zonaTrabajo = $request->input("zone");
        $objetivo = $request->input("objective");
        // Obtenemos toda la información de los ejercicios
        $idEjercicios = $request->input("idEjercicio");
        // Estos son los ejercicios que hay en la base de datos
        $idEjerciciosActuales = $request->input("idEjercicioActuales");
        $nombreEjercicios = $request->input("nameExercise");
        $descripcionEjercicios = $request->input("descripcionEjercicio");
        $repeticionesEjercicios = $request->input("repeticiones");
        // $fotosEjercicios = $request->input("foto");
        $diasEjercicios = $request->input("dias");
        $foto = $request->file("foto");

        $dieta = $request->file("diet");
        $linkVideo = $request->input("link");
        // Convertimos el link de youtube a embed
        $linkEmbed = str_replace("watch?v=", "embed/", $linkVideo);
        if($linkEmbed == '') {
            $linkEmbed = null;
        }
        /* Si el checkbox de eliminar dieta está marcador la eliminamos,
            sino cogemos el input */
        if ($request->input("sinDieta") != "sinDieta" || $dieta == null) {
            $nameDiet = null;
        } else {
            $this->uploadService->uploadPdf($dieta);
            $nameDiet = $dieta->getClientOriginalName();
        }
        // Obtenemos el id de dieta
        $idRutina = $request->input("idRutina");
        $idDetalleRutinas = $this->detalleRutinaModel->getDetalleRutinaById($idRutina);
        //return $idDetalleRutinas[0]->idDetalleRutina;
        // Llamamos al modelo
        $successRutina = $this->rutinaModel->modificarRutina($idRutina, $titulo, $descripcion, $duracion,
            $dificultad, $materiales, $zonaTrabajo, $objetivo, $linkEmbed, $nameDiet);

        $successEliminarDetalleRutina = $this->detalleRutinaModel->eliminarDetalleRutina($idDetalleRutinas, $idRutina, $idEjercicios, $idEjerciciosActuales, $nombreEjercicios, $descripcionEjercicios, $repeticionesEjercicios, $diasEjercicios);
        for ($i = 0; $i < count($idEjercicios); $i++) {
            for($t = 0; $t < count($idEjerciciosActuales); $t++) {
                //Si se encuentra el id en la array de los que se mantienen, no borra
                if ($idEjercicios[$i] == $idEjerciciosActuales[$t]) {
                    $detalleRutinaByEjercicio = $this->detalleRutinaModel->getDetalleRutinaByIdAndIdEjercicio($idRutina, $idEjercicios[$i]);
                    $getEjercicio = $this->ejercicioModel->getEjercicioByID($idEjerciciosActuales[$i]);
                    //return $getEjercicio->idEjercicio;
                    $modificarEjercicio = $this->ejercicioModel->modificarEjercicio($getEjercicio, $nombreEjercicios[$i], $descripcionEjercicios[$i]);
                    $successModificarDetalleRutina = $this->detalleRutinaModel->modificarDetalleRutina($idDetalleRutinas, $idRutina, $detalleRutinaByEjercicio, $idEjercicios[$i], $idEjerciciosActuales, $nombreEjercicios, $descripcionEjercicios, $repeticionesEjercicios[$i], $diasEjercicios[$i]);
                    //return $successModificarDetalleRutina;
                }

            }

        }

        $this->uploadService = $UploadFileService;

        if (count($idEjercicios) < count($idEjerciciosActuales)) {
            $nuevoEjercicio = array_diff($idEjerciciosActuales, $idEjercicios);
            $posicionesNuevosEjercicios = array();
            while ($value = current($nuevoEjercicio)) {
                if (is_string($value)) {
                    array_push($posicionesNuevosEjercicios, key($nuevoEjercicio));
                }
                next($nuevoEjercicio);
            }
            sort($nuevoEjercicio);
            //var_dump($posicionesNuevosEjercicios[0]);
            //var_dump($nuevoEjercicio);
            //return $foto;
            for($i = 0; $i < count($nuevoEjercicio); $i++) {
                try {
                    if (!empty($foto[$posicionesNuevosEjercicios[$i]])) {
                        $this->uploadService->uploadExerciseImg($foto[$posicionesNuevosEjercicios[$i]]);
                        $fotoNombre = $request->foto[$posicionesNuevosEjercicios[$i]]->getClientOriginalName();
                        $this->ejercicioModel->saveEjercicio($nombreEjercicios[$posicionesNuevosEjercicios[$i]], $descripcionEjercicios[$posicionesNuevosEjercicios[$i]], $fotoNombre);
                    } else {

                        return back()->withError("Introduce una foto de ejercicio")->withInput();
                    }
                } catch (UploadFileException $exception) {

                    return back()->withError($exception->mesage())->withInput();
                }
                //$this->ejercicioModel->saveEjercicio($nombreEjercicios[$i], $descripcionEjercicios[$i], $fotoNombre);
                $lastIdExercise = $this->ejercicioModel->lastIdEjercicio();
                $eachDay = explode(',', $diasEjercicios[$posicionesNuevosEjercicios[$i]]);
                //save detalleRutina
                for($z = 0; $z < count($eachDay); $z++) {
                    $especificDay = $eachDay[$z];
                    $this->detalleRutinaModel->saveDetalleRutina($idRutina, $lastIdExercise->idEjercicio, $especificDay, $repeticionesEjercicios[$posicionesNuevosEjercicios[$i]]);
                }
            }

        }
        $dias = $this->rutinaModel->diasRutina($idRutina);
        $meses = $this->rutinaModel->mesesRutina($idRutina);
        $diasTotales = $dias * 4 * $meses[0]->duracion;
        $rutinasSuscritas = $this->rutinaxusuarioModel->getRutinaxusuarioByID($idRutina);
        var_dump($rutinasSuscritas);
        $this->rutinaxusuarioModel->modificarRutinasxUsuario($rutinasSuscritas, $diasTotales);


        //$successModificarDetalleRutina = $this->detalleRutinaModel->modificarDetalleRutina($idDetalleRutinas, $idRutina, $idEjercicios, $idEjerciciosActuales, $nombreEjercicios, $descripcionEjercicios, $repeticionesEjercicios, $diasEjercicios);
        //$successAnadirDetalleRutina = $this->detalleRutinaModel->anadirDetalleRutina($idDetalleRutinas, $idRutina, $idEjercicios, $idEjerciciosActuales, $nombreEjercicios, $descripcionEjercicios, $repeticionesEjercicios, $diasEjercicios);
        //var_dump($successDetalleRutina);
        //Comprobamos si queda algun ejercicio por incluir dentro del array
        /* if (count($successDetalleRutina) > 0) {
            for ($i = 0; $i < count($successDetalleRutina); $i++) {
                try {
                    if (!empty($foto[$i])) {
                        $this->uploadService->uploadExerciseImg($foto[$i]);
                        $fotoNombre = $request->foto[$i]->getClientOriginalName();
                        $this->ejercicioModel->saveEjercicio($nameExercise[$i], $descripcionEjercicio[$i], $fotoNombre);
                    } else {
                        return back()->withError("Introduce una foto de ejercicio")->withInput();
                    }
                } catch (UploadFileException $exception) {
                    return back()->withError($exception->message())->withInput();
                }
                $lastIdExercise = $this->ejercicioModel->lastIdEjercicio();
                $eachDay = explode(',', $dias[$i]);
                //save detalleRutina
                for($z = 0; $z < count($eachDay); $z++) {
                    $especificDay = $eachDay[$z];
                    $this->detalleRutinaModel->saveDetalleRutina($idRutina, $lastIdExercise->idEjercicio, $especificDay, $repeticiones[$i]);
                }
            }
        } */

        //return 1;
        // Devolvemos la vista de descripción rutina
        return redirect('entrenador/descripcionRutina?idRutina='.$idRutina);
    }
}
