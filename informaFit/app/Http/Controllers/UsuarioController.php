<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RequestAjustes;
use App\Models\Usuario;
use App\Models\Rutina;
use App\Models\Rutinasxusuario;

class UsuarioController extends Controller
{
    // Declaración de variables
    public $usuarioModel;
    public $rutinaModel;
    public $rutinasxusuarioModel;

    public function __construct() {
        $this->usuarioModel = new Usuario();
        $this->rutinaModel = new Rutina();
        $this->rutinasxusuarioModel = new Rutinasxusuario();
    }

    /**
     * dataUsuarioCliente
     * 
     * Mostrar los datos del usuario cliente
     */
    public function dataUsuarioCliente(Request $request) {
        // Id usuario
        $idUsuario = $request->session()->get('usuario')['id'];
        return view("cliente.perfilCliente")->with(['data' => $this->usuarioModel->dataUsuario($idUsuario)]);
    }

    /**
     * dataUsuarioEntrenador
     * 
     * Mostrar los datos del usuario entrenador
     */
    public function dataUsuarioEntrenador(Request $request) {
        // Id usuario
        $idUsuario = $request->session()->get('usuario')['id'];
        return view("entrenador.perfilEntrenador")->with(['data' => $this->usuarioModel->dataUsuario($idUsuario)]);
    }

    /**
     * misRutinasCliente
     * 
     * Mostrar las rutinas en las que un cliente
     * está suscrito
     */
    public function misRutinasCliente(Request $request) {
        // Guardamos las rutinas suscritas de un cliente
        $idCliente = $request->session()->get('usuario')['id'];
        $rutinasCliente = $this->rutinaModel->misRutinasCliente($idCliente);
        return view("cliente.misRutinasCliente", ['rutinas' => $rutinasCliente]);
    }

    /**
     * showAjustesCliente
     * 
     * Mostrar vista de ajustes cliente
     */
    public function showAjustesCliente(Request $request) {
        return view("cliente.ajustesCliente");
    }

    /**
     * showAjustesEntrenador
     * 
     * Mostrar vista de ajustes entrenador
     */
    public function showAjustesEntrenador(Request $request) {
        return view("entrenador.ajustesEntrenador");
    }

    /**
     * modificarPassword
     * 
     * Cambiar contraseña del usuario
     */
    public function modificarPassword(RequestAjustes $request) {
        $passwordActual = $request->input("passwordActual");
        $password = $request->input("password");
        // Obtenemos el usuario
        $idUsuario = $request->session()->get('usuario')['id'];
        // Obtenemos el tipo de usuario
        $tipoUsuario = $request->session()->get('tipoUsuario');
        // $usuario = Usuario::select('idUsuario')->where("idUsuario", "=", $idUsuario)->get();
        $usuarioObject = Usuario::find($idUsuario);
        // Comprobamos que la contraseña introducida por el usuario es la actual
        if ($passwordActual != $usuarioObject->contrasena) {
            return back()->withError("La contraseña actual introducida es incorrecta")->withInput();
        } 
        // Modificamos la contraseña
        $usuarioObject->contrasena = $password;
        $success = $usuarioObject->save();
        if ($tipoUsuario == "cliente") {
            return redirect()->action([UsuarioController::class, 'showAjustesCliente'], ['success' => $success]);
        } else {
            return redirect()->action([UsuarioController::class, 'showAjustesEntrenador'], ['success' => $success]);
        }
    }

    /**
     * darseBaja
     * 
     * ELimina usuario
     */
    public function darseBaja(RequestAjustes $request) {
        $password = $request->input("password");
        // Obtenemos el usuario
        $idUsuario = $request->session()->get('usuario')['id'];
        // Obtenemos el tipo de usuario
        $tipoUsuario = $request->session()->get('tipoUsuario');
        // $usuario = Usuario::select('idUsuario')->where("idUsuario", "=", $idUsuario)->get();
        $usuarioObject = Usuario::find($idUsuario);
        // Password usuario
        $usuarioPassword = $usuarioObject->contrasena;
        // Si no son iguales enviamos un mensaje de error
        if ($usuarioPassword != $password) {
            return back()->withError("Contraseña introducida incorrecta")->withInput();
        } else {
            // Eliminamos las variables de sesion
            $request->session()->flush();
            $usuarioObject->delete();
            return redirect('/');
        }
    }

    /**
     * suscripcionRutina
     * 
     * Muestra el formulario para suscribirse a una rutina
     */
    public function suscripcionRutina(Request $request) {
        // Obtenemos el id de la rutina y su información
        $idRutina = $request->input("idRutina");
        $rutina = $this->rutinaModel->descripcionRutina($idRutina)->get();
        $idUsuario = $request->session()->get('usuario')['id'];
        // Calculamos cuantos dias totales tiene la rutina
        $dias = $this->rutinaModel->diasRutina($idRutina);
        $meses = $this->rutinaModel->mesesRutina($idRutina);
        $diasTotales = $dias * 4 * $meses[0]->duracion;
        // Añadimos una nueva fila en la tabla rutinasxusuarios
        $success = $this->rutinasxusuarioModel->saveRutinasxUsuario($idRutina, $idUsuario, $diasTotales);
        return redirect()->back();
    }

    /**
     * añadirDiaCompletado
     * 
     * Sumamaos 1 al campo diaCompletado de la tabla rutinasxusuarios
     */
    public function añadirDiaCompletado(Request $request) {
        // Declaración de variables
        $idRutina = $request->input('idRutina');
        $idUsuario = $request->session()->get('usuario')['id'];
        // Llamamos al modelo Rutinasxusuario
        $rutinaObj = $this->rutinasxusuarioModel->getSuscripcionCliente($idUsuario, $idRutina)->get();
        // Modificamos el campo diasCompletados. Si diasCompletados y diasTotales son iguales reiniciamos el progreso
        if ($rutinaObj[0]->diasCompletados == $rutinaObj[0]->diasTotales) {
            $rutinaObj[0]->diasCompletados = 0;
            $rutinaObj[0]->progreso = 0;
        } else {
            $rutinaObj[0]->diasCompletados += 1;
            // Modificamos el campo progreso
            $rutinaObj[0]->progreso = $rutinaObj[0]->diasCompletados * 100 / $rutinaObj[0]->diasTotales;
        }
        $success = $rutinaObj[0]->save();
        return redirect()->back();
    }

}
