<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class checkEntrenador
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $sessionUser = $request->session()->get("tipoUsuario");
        if ($sessionUser != "entrenador") {
            return redirect("/login");
        }
        return $next($request);
    }
}
