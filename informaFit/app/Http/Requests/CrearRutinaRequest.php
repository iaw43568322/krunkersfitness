<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CrearRutinaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            "title" => "required",
            "description" => "required", 
            "duration" => "required",
            "difficulty" => "required",
            "material" => "required",
            "zone" => "required",
            "objective" => "required"
        ];

        // Comprobar nombre del ejercicio
        foreach($this->request->get('nameExercise') as $key => $val) {
            $rules['nameExercise.'.$key] = 'required';       
        }

        // Comprobar descripcion del ejercicio
        foreach($this->request->get('descripcionEjercicio') as $key => $val) {
            $rules['descripcionEjercicio.'.$key] = 'required';       
        }

        // Comprobar descripcion del ejercicio
        foreach($this->request->get('repeticiones') as $key => $val) {
            $rules['repeticiones.'.$key] = 'required';       
        }

        // Comprobar descripcion del ejercicio
        foreach($this->request->get('dias') as $key => $val) {
            $rules['dias.'.$key] = 'required';       
        }
        return $rules;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $messages = [
            'title.required' => 'El título es obligatorio',
            'description.required' => 'La descripción es obligatoria',
            'duration.required' => 'La duración es obligatoria',
            'difficulty.required' => 'La dificultad es obligatoria',
            'descripcionEjercicio.required' => "La descripción es obligatoria",
            'repeticiones.required' => "Las repeticiones son obligatorias",
            'dias.required' => 'Los dias son requeridos',
            'material.required' => 'Los materiales son obligatorios',
            'zone.required' => 'La zona es obligatoria',
            'objective.required' => 'El objetivo es obligatorio'
        ];
        // Mensaje nombre ejercicio
        foreach($this->request->get('nameExercise') as $key => $val)
        {
            $messages['nameExercise.'.$key.'.required'] = 'El nombre del ejercicio debe tener un nombre';
        }
        // Mensaje descripcion ejercicio
        foreach($this->request->get('descripcionEjercicio') as $key => $val)
        {
            $messages['descripcionEjercicio.'.$key.'.required'] = 'La descripción del ejercicio es obligatoria';
        }
        // Mensaje repeticiones ejercicio
        foreach($this->request->get('repeticiones') as $key => $val)
        {
            $messages['repeticiones.'.$key.'.required'] = 'Las repeticiones del ejercicio son obligatorios';
        }
        // Mensaje dias ejercicio
        foreach($this->request->get('dias') as $key => $val)
        {
            $messages['dias.'.$key.'.required'] = 'Las días del ejercicio son obligatorios';
        }
        return $messages;
    }
}
