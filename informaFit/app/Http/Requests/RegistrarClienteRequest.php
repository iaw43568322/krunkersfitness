<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class registrarClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre" => "required",
            "apellidos" => "required",
            "descripcion" => "required",
            "email" => "required|email:rfc,dns",
            "password" => "required|min:6|required_with:passwordAgain|same:passwordAgain",
            "passwordAgain" => "required"
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es obligatorio',
            'apellidos.required' => 'Los apellidos son obligatorios',
            'email.required' => 'El email es obligatorio',
            'email.email' => 'El email introducido no es válido',
            'password.min' => 'La contraseña tiene que tener 6 caracteres como mínimo',
            'password.same' => 'Las contraseñas no coinciden'
        ];
    }

}
