<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class registrarEntrenadorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre" => "required",
            "apellidos" => "required",
            "dni" => "required",
            "email" => "required|email:rfc,dns",
            "telefono" => "required",
            "descripcion" => "required",
            "password" => "min:6|required_with:passwordAgain|same:passwordAgain",
            "passwordAgain" => "min:6"
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es obligatorio',
            'apellidos.required' => 'Los apellidos son obligatorios',
            'dni.required' => 'El dni es obligatorio',
            'email.required' => 'El email es obligatorio',
            'email.email' => 'El email introducido no es válido',
            'telefono.required' => 'El telefono es obligatorio',
            'descripcion.required' => 'La descripción es obligatoria',
            'password.min' => 'La contraseña tiene que tener 6 caracteres como mínimo',
            'password.same' => 'Las contraseñas no coinciden'
        ];
    }
}
