<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestAjustes extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "passwordActual" => "required",
            "password" => "required|min:6|required_with:passwordVerify|same:passwordVerify",
            "passwordVerify" => "required"
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'passwordActual.required' => 'Debes introducir tu contraseña actual',
            'password.required' => 'La contraseña es obligatoria',
            'password.min' => 'La contraseña tiene que tener 6 caracteres como mínimo',
            'password.same' => 'Las contraseñas no coinciden'
        ];
    }
}
