<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public function saveCliente($nombre, $descripcion, $apellidos, $email, $password) {
        $newCliente = new Cliente();
        $newCliente->nombre = $nombre;
        $newCliente->descripcion = $descripcion;
        $newCliente->apellidos = $apellidos;
        $newCliente->email = $email;
        $newCliente->password = $password;
        return $newCliente->save();
    }

}
