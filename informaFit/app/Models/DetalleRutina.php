<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetalleRutina extends Model
{
    protected $primaryKey = "idDetalleRutina";
    protected $table = 'detalleRutinas';
    public function saveDetalleRutina($idRutina, $idEjercicio, $dia, $repeticiones) {
        // Creamos una nueva instancia de Usuario
        $newDetalleRutina = new DetalleRutina();
        // Guardamos los diferentes atributos al nuevo usuario
        $newDetalleRutina->idRutina = $idRutina;
        $newDetalleRutina->idEjercicio = $idEjercicio;
        $newDetalleRutina->dia = $dia;
        $newDetalleRutina->repeticiones = $repeticiones;
        // Lo guardamos en la base de datos
        return $newDetalleRutina->save();
    }

    /**
     * getDetalleRutinaById
     *
     * Recoger detalles dado el id de una rutina
     */
    public function scopeGetDetalleRutinaById($query, $idRutina) {
        $detalleRutinas = $query->where("idRutina", "=", $idRutina)->get();
        return $detalleRutinas;
    }

    public function scopeGetDetalleRutinaByIdAndIdEjercicio($query, $idRutina, $idEjercicio) {
        $detalleRutinasByExercise = $query->where("idRutina", "=", $idRutina)->where("idEjercicio", "=", $idEjercicio)->get();
        return $detalleRutinasByExercise;
    }

    /**
     * modificarDetalleRutina
     *
     * Modifica las filas de detalleRutina de una rutina en concreto
     */
    public function scopeModificarDetalleRutina($query, $idDetalleRutinas, $idRutina, $detalleRutinaByEjercicio, $idEjercicios, $idEjerciciosActuales, $nombreEjercicios, $descripcionEjercicios, $repeticionesEjercicios, $diasEjercicios) {
            $modificar = array();
            // Consulta a la base de datos para obtener los id de detalle rutina
            for ($i = 0; $i < count($detalleRutinaByEjercicio); $i++) {
                array_push($modificar, $detalleRutinaByEjercicio[$i]->dia);
                //var_dump($detalleRutinaByEjercicio[$i]->idDetalleRutina);
            }
            $eachDay = explode(',', $diasEjercicios);
            //var_dump($eachDay);
            if(count($modificar) < count($eachDay)) {
                $nuevoDia = array_diff($eachDay, $modificar);
                var_dump($nuevoDia);
                var_dump("hola");
                sort($nuevoDia);
                for ($y = 0; $y < count($nuevoDia); $y++) {
                    $idEjercicio = $detalleRutinaByEjercicio[0]->idEjercicio;
                    $repeticiones = $detalleRutinaByEjercicio[0]->repeticiones;
                    //var_dump($nuevoDia[$y]);
                    $especificDay = $nuevoDia[$y];
                    $newDetalleRutina = new DetalleRutina();
                    // Guardamos los diferentes atributos al nuevo usuario
                    $newDetalleRutina->idRutina = $idRutina;
                    $newDetalleRutina->idEjercicio = $idEjercicio;
                    $newDetalleRutina->dia = $especificDay;
                    $newDetalleRutina->repeticiones = $repeticiones;
                    // Lo guardamos en la base de datos
                    $newDetalleRutina->save();
                }
            }
            // Bucle para obtener 1 objeto para cada ejercicio que haya
            for ($i = 0; $i < count($detalleRutinaByEjercicio); $i++) {

                // Obtenemos el objeto
                $detalleRutinaObj = DetalleRutina::find($detalleRutinaByEjercicio[$i]->idDetalleRutina);

                //return $idDetalleRutinas[$i]->idDetalleRutinas;
                // Si esta en el array de ejercicios actuales, lo mantenemos

                    // Cambiamos los datos

                    $eachDay = explode(',', $diasEjercicios);

                        $especificDay = $eachDay[$i];
                        //var_dump($especificDay);
                        $repeticiones = $repeticionesEjercicios[0];
                        //var_dump($idDetalleRutinas[$i]->idDetalleRutina);
                        $detalleRutinaObj->dia = $especificDay;
                        $detalleRutinaObj->repeticiones = $repeticionesEjercicios;
                        $detalleRutinaObj->save();


                //Volvemos al antiguo valor para comprobar el siguiente

            }
            return $detalleRutinaObj;
    }

    public function scopeAnadirDetalleRutina($query, $idDetalleRutinas, $idRutina, $idEjercicios, $idEjerciciosActuales, $nombreEjercicios,
    $descripcionEjercicios, $repeticionesEjercicios, $diasEjercicios) {

    }

    public function scopeEliminarDetalleRutina($query, $idDetalleRutinas, $idRutina, $idEjercicios, $idEjerciciosActuales, $nombreEjercicios,
    $descripcionEjercicios, $repeticionesEjercicios, $diasEjercicios) {
        $delete = true;
            // Consulta a la base de datos para obtener los id de detalle rutina

            // Bucle para obtener 1 objeto para cada ejercicio que haya
            for ($i = 0; $i < count($idEjercicios); $i++) {
                for($t = 0; $t < count($idEjerciciosActuales); $t++) {
                    //Si se encuentra el id en la array de los que se mantienen, no borra
                    if ($idEjercicios[$i] == $idEjerciciosActuales[$t]) {
                        $delete = false;
                    }

                }

                // Obtenemos el objeto
                $detalleRutinaObj = DetalleRutina::find($idDetalleRutinas[$i]->idDetalleRutina);
                //return $idDetalleRutinas[$i]->idDetalleRutinas;
                // Si esta en el array de ejercicios actuales, lo mantenemos
                if ($delete) {
                    //var_dump("elimino");
                    $detalleRutinaObj->delete();

                }

                //Volvemos al antiguo valor para comprobar el siguiente
                $delete = true;
            }
            return $idEjerciciosActuales;
    }
}
