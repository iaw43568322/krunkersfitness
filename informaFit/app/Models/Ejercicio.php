<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ejercicio extends Model
{
    public function saveEjercicio($nombre, $descripcion, $foto) {
        // Creamos una nueva instancia de Usuario
        $newEjercicio = new Ejercicio();
        // Guardamos los diferentes atributos al nuevo usuario
        $newEjercicio->nombre = $nombre;
        $newEjercicio->descripcion = $descripcion;
        $newEjercicio->foto = "./assets/img/ejerciciosFotos/".$foto;
        // Lo guardamos en la base de datos
        return $newEjercicio->save();
    }

    public function modificarEjercicio($ejercicioActual, $nombre, $descripcion) {

        //modificamos sus valores
        $ejercicioActual->nombre = $nombre;
        $ejercicioActual->descripcion = $descripcion;

        $ejercicioActual->save();
    }

    public function lastIdEjercicio() {
        return $this->latest('idEjercicio')->first();
    }

    public function scopeCountEjercicios() {
        return $this->count();
    }

    public function scopeGetEjercicioByID($query, $idEjercicio) {
        $ejercicio = $query->where("idEjercicio", "=", $idEjercicio)->first();
        return $ejercicio;
    }
}
