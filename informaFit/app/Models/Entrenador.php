<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entrenador extends Model
{
    public function saveEntrenador($nombre, $apellidos, $dni, $email, $telefono, $descripcion, $password) {
        $newEntrenador = new Entrenador();
        $newEntrenador->nombre = $nombre;
        $newEntrenador->apellidos = $apellidos;
        $newEntrenador->dni = $dni;
        $newEntrenador->email = $email;
        $newEntrenador->telefono = $telefono;
        $newEntrenador->descripcion = $descripcion;
        $newEntrenador->password = $password;
        return $newEntrenador->save();
    }
}
