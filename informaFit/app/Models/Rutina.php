<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rutina extends Model
{
    protected $primaryKey = "idRutina";

    public function saveRutina($idUsuario, $titulo, $descripcion, $duracion, $link_video, $dificultad, $objetivo, $materiales, $zonaTrabajo, $pdfDieta) {
        // Creamos una nueva instancia de Usuario
        $newRutina = new Rutina();
        // Guardamos los diferentes atributos al nuevo usuario
        $newRutina->idUsuario = $idUsuario;
        $newRutina->titulo = $titulo;
        $newRutina->descripcion = $descripcion;
        $newRutina->duracion = $duracion;
        $newRutina->link_video = $link_video;
        $newRutina->dificultad = $dificultad;
        $newRutina->objetivo = $objetivo;
        $newRutina->materiales = $materiales;
        $newRutina->zonaTrabajo = $zonaTrabajo;
        if (!empty($pdfDieta)) {
            $newRutina->pdfDieta = "./assets/docs/".$pdfDieta;
        }
        // Lo guardamos en la base de datos
        return $newRutina->save();
    }


    // Método para hacer el join entre rutina y entrenador
    public function rutinaEntrenador() {
        return $this->join('usuarios', 'rutinas.idUsuario', '=', 'usuarios.idUsuario')
            ->get(['rutinas.*', 'rutinas.descripcion as rutinaDesc', 'usuarios.*', 'usuarios.nombre as usuarioNom']);
    }

    public function scopeDificultad($query, $dificultad) {
        return $query->whereIn("dificultad", $dificultad);
    }

    public function scopePalabraClave($query, $palabraClave) {
        return $query->join('usuarios', 'rutinas.idUsuario', '=', 'usuarios.idUsuario')
            ->where('titulo', 'LIKE', '%'.$palabraClave.'%')
            ->get(['rutinas.*', 'rutinas.descripcion as rutinaDesc', 'usuarios.*', 'usuarios.nombre as usuarioNom']);
    }

    public function scopeDuracion($query, $duracion) {
        return $query->where("duracion", "=", $duracion);
    }

    public function scopeZonaTrabajo($query, $zonaTrabajo) {
        return $query->whereIn("zonaTrabajo", $zonaTrabajo);
    }

    public function scopeOrder($query, $orden) {
        if ($orden == "created_at") {
            return $query->orderBy('rutinas.created_at', "desc");
        } else if ($orden == "progreso") {
            return $query->orderBy('rutinasxusuarios.progreso', "desc");
        }
        return $query->orderBy($orden, "asc");

    }

    public function scopeObjetivo($query, $objetivo) {
        return $query->whereIn("objetivo", $objetivo);
        // return $this->join('usuarios', 'rutinas.idUsuario', '=', 'usuarios.idUsuario')
        //     ->get(['rutinas.*', 'rutinas.descripcion as rutinaDesc', 'usuarios.*', 'usuarios.nombre as usuarioNom']);
    }

    // Método para mostrar una sola rutina
    public function scopeDescripcionRutina($query, $idRutina) {
        return $query->where('rutinas.idRutina', '=', $idRutina);
        // return $query->join('detalleRutinas', 'rutinas.idRutina', '=', 'detalleRutinas.idRutina')->where('rutinas.idRutina', '=', $idRutina);
    }

    // Método para agrupar los dias
    public function scopeDias($query, $idRutina) {
        return $query->join('detalleRutinas', 'rutinas.idRutina', '=', 'detalleRutinas.idRutina')
            ->select('detalleRutinas.dia')->distinct()->where('rutinas.idRutina', '=', $idRutina)->orderBy('dia', 'asc');
    }

    // Método para agrupar los ejercicios
    public function scopeEjercicios($query, $idRutina) {
        return $query->join('detalleRutinas', 'rutinas.idRutina', '=', 'detalleRutinas.idRutina')
            ->join('ejercicios', 'detalleRutinas.idEjercicio', '=', 'ejercicios.idEjercicio')
            ->where('rutinas.idRutina', '=', $idRutina);

    }

    // Método que devuelve todas las rutinas de un entrenador en concreto
    public function scopeMisRutinasEntrenador($query, $idEntrenador) {
        // Devolvemos todas las rutinas del entrenador que tenga el mismo email que $emailEntrenador
        return $query->join('usuarios', 'rutinas.idUsuario', '=', 'usuarios.idUsuario')
            ->where('usuarios.idUsuario', '=', $idEntrenador)
            ->get(['usuarios.*', 'usuarios.nombre as usuarioNom', 'rutinas.*', 'rutinas.descripcion as rutinaDesc']);

    }

    public function lastIdRutina() {
        return $this->latest('idRutina')->first();
    }

    // Método que devuelve las rutinas en que un cliente está suscrito
    public function scopeMisRutinasCliente($query, $idCliente) {
        // Consulta a la base de datos
        return $query->join('rutinasxusuarios', 'rutinas.idRutina', '=', 'rutinasxusuarios.idRutina')
            ->join('usuarios', 'rutinasxusuarios.idUsuario', '=', 'usuarios.idUsuario')
            ->join('usuarios as entrenador', 'rutinas.idUsuario', '=', 'entrenador.idUsuario')
            ->where('usuarios.idUsuario', '=', $idCliente)
            ->get(['usuarios.*', 'entrenador.nombre as nombreEntrenador', 'entrenador.apellidos as apellidosEntrenador', 'rutinas.*', 'rutinas.descripcion as rutinaDesc', 'rutinasxusuarios.*', 'rutinas.created_at as rutinaFecha']);
    }

    // Método para filtrar por palabra clave las rutinas de un cliente
    public function scopeMisRutinasClientePalabra($query, $idCliente, $keyword) {
        // return $query->misRutinasCliente($idCliente)->where('titulo', 'LIKE', '%'.$keyword.'%')->get();
        return $query->join('rutinasxusuarios', 'rutinas.idRutina', '=', 'rutinasxusuarios.idRutina')
        ->join('usuarios', 'rutinasxusuarios.idUsuario', '=', 'usuarios.idUsuario')
        ->join('usuarios as entrenador', 'rutinas.idUsuario', '=', 'entrenador.idUsuario')
        ->where('usuarios.idUsuario', '=', $idCliente)->where('titulo', 'LIKE', '%'.$keyword.'%')
        ->get(['rutinas.*', 'rutinas.descripcion as rutinaDesc', 'usuarios.*', 'entrenador.nombre as nombreEntrenador', 'entrenador.apellidos as apellidosEntrenador']);
    }

    // Método para ordenar las rutinas de un cliente
    public function scopeMisRutinasClienteOrdenar($query, $idCliente, $ordenarPor) {
        return $query->join('rutinasxusuarios', 'rutinas.idRutina', '=', 'rutinasxusuarios.idRutina')
        ->join('usuarios', 'rutinasxusuarios.idUsuario', '=', 'usuarios.idUsuario')
        ->join('usuarios as entrenador', 'rutinas.idUsuario', '=', 'entrenador.idUsuario')
        ->where('usuarios.idUsuario', '=', $idCliente)
        ->orderBy('rutinas.'.$ordenarPor, 'desc')
        ->get(['rutinas.*', 'rutinas.descripcion as rutinaDesc', 'usuarios.*', 'entrenador.nombre as nombreEntrenador',
            'entrenador.apellidos as apellidosEntrenador']);
    }

    // Método para agrupar los ejercicios
    public function scopeEjerciciosDistinct($query, $idRutina) {
        return $query->join('detalleRutinas', 'rutinas.idRutina', '=', 'detalleRutinas.idRutina')
            ->join('ejercicios', 'detalleRutinas.idEjercicio', '=', 'ejercicios.idEjercicio')
            ->select('ejercicios.*', 'detalleRutinas.repeticiones')
            ->distinct()
            ->where('rutinas.idRutina', '=', $idRutina);

    }

    // Método para saber cuantos dias tiene una rutina en total
    public function scopeDiasRutina($query, $idRutina) {
        return $query->join('detalleRutinas', 'rutinas.idRutina', '=', 'detalleRutinas.idRutina')
            ->join('ejercicios', 'detalleRutinas.idEjercicio', '=', 'ejercicios.idEjercicio')
            ->select('detalleRutinas.dia')
            ->distinct()
            ->where('rutinas.idRutina', '=', $idRutina)
            ->count();

    }

    public function scopeDiasRutinaByEjercicio($query, $idRutina) {
        return $query->join('detalleRutinas', 'rutinas.idRutina', '=', 'detalleRutinas.idRutina')
            ->join('ejercicios', 'detalleRutinas.idEjercicio', '=', 'ejercicios.idEjercicio')
            ->select('detalleRutinas.dia', 'detalleRutinas.idEjercicio')
            ->where('rutinas.idRutina', '=', $idRutina);

    }

    // Método para saber cuantos meses dura una rutina
    public function scopeMesesRutina($query, $idRutina) {
        // Devolvemos el campo duracion de una rutina en concreto
        return $query->select('duracion')->where('idRutina', '=', $idRutina)->get();
    }

    // Método para modificar rutina
    public function modificarRutina($idRutina, $titulo, $descripcion, $duracion,
        $dificultad, $materiales, $zonaTrabajo, $objetivo, $linkEmbed, $nameDiet) {
        // Guardamos el objeto de rutina para modificar los datos
        $rutinaObj = Rutina::find($idRutina);
        // Cambiamos todos los datos
        $rutinaObj->titulo = $titulo;
        $rutinaObj->descripcion = $descripcion;
        $rutinaObj->duracion = $duracion;
        $rutinaObj->dificultad = $dificultad;
        $rutinaObj->materiales = $materiales;
        $rutinaObj->zonaTrabajo = $zonaTrabajo;
        $rutinaObj->objetivo = $objetivo;
        $rutinaObj->link_video = $linkEmbed;
        if($nameDiet != null) {
            $rutinaObj->pdfDieta = "./assets/docs/".$nameDiet;
        } else {
            $rutinaObj->pdfDieta = null;
        }

        return $rutinaObj->save();
    }
}
