<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rutinasxusuario extends Model
{
    protected $primaryKey = "idRutinaxusuario";

    /**
     * getRutinaxusuarioByID
     *
     * Recoger suscripcion segun el id de rutina
     */
    public function scopeGetRutinaxusuarioByID($query, $idRutina) {
        $suscripcion = $query->where("idRutina", "=", $idRutina)->get();
        return $suscripcion;
    }

    /**
     * scopeGetSuscripcionCliente
     * Obtener una suscripción de un cliente en concreto
    */
    public function scopeGetSuscripcionCliente($query, $idCliente, $idRutina) {
        return $query->where("idUsuario", "=", $idCliente)->where("idRutina", "=", $idRutina);
    }

    /**
     * scopeGetSuscripcionesCliente
     *
     * Obtener TODAS las suscripciones de un cliente en concreto
     */
    public function scopeGetSuscripcionesCliente($query, $idCliente) {
        return $query->where('idUsuario', "=", $idCliente);
    }

    /**
     * saveRutinasxUsuario
     *
     * Añadir una fila cuando un usuario se suscribe a una rutina
     */
    public function saveRutinasxUsuario($idRutina, $idUsuario, $diasTotales) {
        // Creamos el objeto rutinasxusuario
        $rutinasxusuarioObj = new Rutinasxusuario();
        // Guardamos los datos
        $rutinasxusuarioObj->idRutina = $idRutina;
        $rutinasxusuarioObj->idUsuario = $idUsuario;
        $rutinasxusuarioObj->diasTotales = $diasTotales;
        $rutinasxusuarioObj->diasCompletados = 0;
        $rutinasxusuarioObj->progreso = 0;
        return $rutinasxusuarioObj->save();
    }
    public function modificarRutinasxUsuario($rutinasSuscritas, $diasTotales) {
        // Creamos el objeto rutinasxusuario
        for ($i = 0; $i < count($rutinasSuscritas); $i++) {
            $rutinaxusuarioObj = Rutinasxusuario::find($rutinasSuscritas[$i]->idRutinaxusuario);
            $rutinaxusuarioObj->diasTotales = $diasTotales;
            $rutinaxusuarioObj->save();
        }
    }
}
