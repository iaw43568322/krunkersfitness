<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $primaryKey = "idUsuario";
    /**
     * saveCliente
     * 
     * Guardar un nuevo usuario cliente
     * 
     */
    public function saveCliente($nombre, $descripcion, $apellidos, $email, $fotoUsuario, $password, $tipo) {
        // Creamos una nueva instancia de Usuario
        $newCliente = new Usuario();
        // Guardamos los diferentes atributos al nuevo usuario
        $newCliente->nombre = $nombre;
        $newCliente->apellidos = $apellidos;
        $newCliente->descripcion = $descripcion;
        $newCliente->dni = null;
        $newCliente->email = $email;
        $newCliente->fotoUsuario = "../assets/img/fotoUsuario/".$fotoUsuario;
        $newCliente->contrasena = $password;
        $newCliente->tipo = $tipo;
        // Lo guardamos en la base de datos
        return $newCliente->save(); 
    }

    /**
     * saveEntrenador
     * 
     * Guardar un nuevo usuario entrenador
     */
    public function saveEntrenador($nombre, $apellidos, $dni, $telefono, $descripcion, $email, $fotoUsuario, $password, $tipo) {
        // Creamos una nueva instancia de Usuario
        $newEntrenador = new Usuario();
        // Guardamos los diferentes atributos al nuevo usuario
        $newEntrenador->nombre = $nombre;
        $newEntrenador->apellidos = $apellidos;
        $newEntrenador->dni = $dni;
        $newEntrenador->telefono = $telefono;
        $newEntrenador->descripcion = $descripcion;
        $newEntrenador->email = $email;
        $newEntrenador->fotoUsuario = "../assets/img/fotoUsuario/".$fotoUsuario;
        $newEntrenador->contrasena = $password;
        $newEntrenador->tipo = $tipo;
        // Lo guardamos en la base de datos
        return $newEntrenador->save();
    }

    /**
     * scopeSearchUser
     * 
     * Comprobar si existe un usuario registrado
     */
    public function scopeCheckUser($query, $email, $password) {
        // Buscamos un registro igual a los parametros pasados
        return $query->where("email", "=", $email)->where("contrasena", "=", $password)->get();
    }

    /**
     * scopeDataUsuario
     * 
     * Recoger datos de un usuario en especifico
     */
    public function scopeDataUsuario($query, $idUsuario) {
        return $query->where("idUsuario", "=", $idUsuario)->get();
    }

    /**
     * scopeCountUsers
     * 
     * contar usuarios cliente
     */
    public function scopeCountUsers($query) {
        return count($this->where("tipo", "=", "cliente")->get());
    }

    /**
     * updatePassword
     */
    // public function updatePassword($password) {
    //     // Obtenemos el usuario
    //     $usuario = Usuario::find();
    // }

}
