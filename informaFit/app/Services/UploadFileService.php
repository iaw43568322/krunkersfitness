<?php

namespace App\Services;

use App\Exceptions\UploadFileException;

class UploadFileService
{

    function uploadFile($file)
    {
        // Creamos una excepción si el fichero está vacio
        if ($file == null) {
            throw new UploadFileException('File not defined');
        }

        //Metodo B (carpeta publica y nombre del file original)
        //guardamos la imagen en public/src/products para que los usuarios puedan
        //tener acceso
        $destinationPath = public_path().'/assets/img/fotoUsuario';
        $originalFile = $file->getClientOriginalName();
        $file->move($destinationPath, $originalFile);
    }

    function uploadExerciseImg($file) {
        // Creamos una excepción si el fichero está vacio
        if ($file == null) {
            throw new UploadFileException('File not defined');
        }

        //Metodo B (carpeta publica y nombre del file original)
        //guardamos la imagen en public/src/products para que los usuarios puedan
        //tener acceso
        $destinationPath = public_path().'/assets/img/ejerciciosFotos';
        $originalFile = $file->getClientOriginalName();
        $file->move($destinationPath, $originalFile);
    }

    function uploadPdf($file) {
        // Creamos una excepción si el fichero está vacio
        if ($file == null) {
            throw new UploadFileException('File not defined');
        }

        //Metodo B (carpeta publica y nombre del file original)
        //guardamos la imagen en public/src/products para que los usuarios puedan
        //tener acceso
        $destinationPath = public_path().'/assets/docs';
        $originalFile = $file->getClientOriginalName();
        $file->move($destinationPath, $originalFile);
    }
}