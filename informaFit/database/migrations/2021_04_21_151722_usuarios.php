<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            // Id user
            $table->increments('idUsuario');
            // Nombre
            $table->string('nombre', 255);
            // Apellidos
            $table->string('apellidos', 255);
            // Descripcion
            $table->text('descripcion', 2000);
            // DNI
            $table->string('dni', 9)->nullable();
            // Email
            $table->string('email', 150)->unique();
            // Contrasena
            $table->string('contrasena', 150);
            // Telefono
            $table->string('telefono', 20)->nullable();
            // Tipo
            $table->string('tipo', 100);
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuario');
    }
}
