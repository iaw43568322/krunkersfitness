<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rutinas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rutinas', function (Blueprint $table) {
            // Id
            $table->increments('idRutina');
            // Id usuario
            $table->integer('idUsuario')->unsigned();
            // Titulo
            $table->string('titulo', 255);
            // Descripcion
            $table->text('descripcion', 2000);
            // Duracion
            $table->integer('duracion');
            // Link video
            $table->string('link_video', 255)->nullable();
            // Dificultad
            $table->string('dificultad', 100);
            // Objetivo
            $table->string('objetivo', 100);
            // Materiales
            $table->string('materiales', 200);
            // Zona de trabajo
            $table->string('zonaTrabajo', 150);
            // pdf
            $table->string('pdfDieta', 150)->nullable();
            // Foreign key usuario
            $table->foreign('idUsuario')->references('idUsuario')->on('usuarios')->onUpdate('cascade')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();

        });
        // Id
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rutina');
    }
}
