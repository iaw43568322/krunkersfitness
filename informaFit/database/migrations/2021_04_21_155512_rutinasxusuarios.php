<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Rutinasxusuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rutinasxusuarios', function (Blueprint $table) {
            // Id
            $table->increments('idRutinaxusuario');
            // Id rutina
            $table->integer('idRutina')->unsigned();
            // Id usuario
            $table->integer('idUsuario')->unsigned();
            // Dias totales
            $table->integer('diasTotales');
            // Dias completados
            $table->integer('diasCompletados');
            // Foreign key rutina
            $table->foreign('idRutina')->references('idRutina')->on('rutinas')->onUpdate('cascade')->onDelete('cascade');
            // Foreign key usuario
            $table->foreign('idUsuario')->references('idUsuario')->on('usuarios')->onUpdate('cascade')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rutinaxusuario');
    }
}
