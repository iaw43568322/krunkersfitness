<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Detallerutinas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalleRutinas', function (Blueprint $table) {
            // Id
            $table->increments('idDetalleRutina');
            // Id rutina
            $table->integer('idRutina')->unsigned();
            // Id ejercicio
            $table->integer('idEjercicio')->unsigned();
            // Dia de la semana (transformar a int...)
            $table->integer('dia');
            // Repeticiones/duracion
            $table->string('repeticiones');
            // Foreign key rutina
            $table->foreign('idRutina')->references('idRutina')->on('rutinas')->onUpdate('cascade')->onDelete('cascade');
            // Foreign key usuario
            $table->foreign('idEjercicio')->references('idEjercicio')->on('ejercicios')->onUpdate('cascade')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalleRutina');
    }
}
