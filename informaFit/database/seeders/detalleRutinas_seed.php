<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class detalleRutinas_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 1
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 1,
            'idEjercicio' => 3,
            'dia' => 1,
            'repeticiones' => '4 x 20 repeticiones',
            'created_at' => Carbon::now()->subMonth(3)->format('Y-m-d H:i:s') 
            
        ));
        // 2
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 1,
            'idEjercicio' => 1,
            'dia' => 1,
            'repeticiones' => '4 x 50 repeticiones',
            'created_at' => Carbon::now()->subMonth(3)->format('Y-m-d H:i:s') 
            
        ));
        // 3
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 1,
            'idEjercicio' => 6,
            'dia' => 2,
            'repeticiones' => '4 x 1 minuto',
            'created_at' => Carbon::now()->subMonth(3)->format('Y-m-d H:i:s') 
            
        ));
        // 4
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 1,
            'idEjercicio' => 4,
            'dia' => 3,
            'repeticiones' => '4 x 20 repeticiones',
            'created_at' => Carbon::now()->subMonth(3)->format('Y-m-d H:i:s') 
            
        ));
        // 5
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 2,
            'idEjercicio' => 8,
            'dia' => 1,
            'repeticiones' => '3 x 12 repeticiones',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 6
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 2,
            'idEjercicio' => 8,
            'dia' => 2,
            'repeticiones' => '3 x 12 repeticiones',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 7
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 2,
            'idEjercicio' => 8,
            'dia' => 4,
            'repeticiones' => '3 x 12 repeticiones',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 8
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 2,
            'idEjercicio' => 9,
            'dia' => 1,
            'repeticiones' => '3 x 21 repeticiones',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 9
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 2,
            'idEjercicio' => 9,
            'dia' => 3,
            'repeticiones' => '3 x 21 repeticiones',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 10
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 2,
            'idEjercicio' => 9,
            'dia' => 5,
            'repeticiones' => '3 x 21 repeticiones',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 11
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 2,
            'idEjercicio' => 10,
            'dia' => 2,
            'repeticiones' => '3 x 15 repeticiones',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 12
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 2,
            'idEjercicio' => 10,
            'dia' => 3,
            'repeticiones' => '3 x 15 repeticiones',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 13
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 2,
            'idEjercicio' => 10,
            'dia' => 5,
            'repeticiones' => '3 x 15 repeticiones',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 14
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 11,
            'dia' => 1,
            'repeticiones' => '5 x 30 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s') 
            
        ));

        // 15
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 11,
            'dia' => 2,
            'repeticiones' => '5 x 25 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s') 
            
        ));

        // 16
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 11,
            'dia' => 3,
            'repeticiones' => '5 x 30 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s') 
            
        ));

        // 17
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 11,
            'dia' => 4,
            'repeticiones' => '5 x 30 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s') 
            
        ));

        // 18
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 12,
            'dia' => 1,
            'repeticiones' => '5 x 20 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s') 
            
        ));

        // 19
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 12,
            'dia' => 2,
            'repeticiones' => '5 x 20 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s') 
            
        ));

        // 20
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 12,
            'dia' => 4,
            'repeticiones' => '5 x 20 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s')
            
        ));

        // 21
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 13,
            'dia' => 2,
            'repeticiones' => '5 x 25 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s') 
            
        ));

        // 22
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 13,
            'dia' => 3,
            'repeticiones' => '5 x 25 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s') 
            
        ));

        // 23
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 3,
            'idEjercicio' => 13,
            'dia' => 4,
            'repeticiones' => '5 x 25 repeticiones',
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s') 
            
        ));

        // 24
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 4,
            'idEjercicio' => 1,
            'dia' => 1,
            'repeticiones' => '3 x 50 repeticiones',
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s') 
            
        ));

        // 25
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 4,
            'idEjercicio' => 1,
            'dia' => 2,
            'repeticiones' => '3 x 50 repeticiones',
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s') 
            
        ));

        // 26
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 4,
            'idEjercicio' => 1,
            'dia' => 3,
            'repeticiones' => '3 x 50 repeticiones',
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s') 
            
        ));

        // 27
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 4,
            'idEjercicio' => 14,
            'dia' => 1,
            'repeticiones' => '3 x 30 repeticiones',
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s') 
            
        ));

        // 28
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 4,
            'idEjercicio' => 14,
            'dia' => 2,
            'repeticiones' => '3 x 30 repeticiones',
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s') 
            
        ));

        // 29
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 4,
            'idEjercicio' => 14,
            'dia' => 3,
            'repeticiones' => '3 x 30 repeticiones',
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s') 
            
        ));

        // 30
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 4,
            'idEjercicio' => 15,
            'dia' => 1,
            'repeticiones' => '3 x 30 repeticiones',
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s') 
            
        ));

        // 31
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 4,
            'idEjercicio' => 15,
            'dia' => 2,
            'repeticiones' => '3 x 30 repeticiones',
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s') 
            
        ));

        // 32
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 4,
            'idEjercicio' => 15,
            'dia' => 3,
            'repeticiones' => '3 x 30 repeticiones',
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s') 
            
        ));

        // 33
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 5,
            'idEjercicio' => 16,
            'dia' => 1,
            'repeticiones' => '4 x 12 repeticiones',
            'created_at' => Carbon::now()->subMonth()->format('Y-m-d H:i:s') 
            
        ));

        // 34
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 5,
            'idEjercicio' => 16,
            'dia' => 3,
            'repeticiones' => '4 x 12 repeticiones',
            'created_at' => Carbon::now()->subMonth()->format('Y-m-d H:i:s') 
            
        ));

        // 35
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 5,
            'idEjercicio' => 17,
            'dia' => 1,
            'repeticiones' => '4 x 15 repeticiones',
            'created_at' => Carbon::now()->subMonth()->format('Y-m-d H:i:s') 
            
        ));

        // 36
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 5,
            'idEjercicio' => 17,
            'dia' => 3,
            'repeticiones' => '4 x 15 repeticiones',
            'created_at' => Carbon::now()->subMonth()->format('Y-m-d H:i:s') 
            
        ));

        // 37
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 5,
            'idEjercicio' => 18,
            'dia' => 1,
            'repeticiones' => '4 x 15 repeticiones',
            'created_at' => Carbon::now()->subMonth()->format('Y-m-d H:i:s') 
            
        ));

        // 38
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 5,
            'idEjercicio' => 17,
            'dia' => 3,
            'repeticiones' => '4 x 15 repeticiones',
            'created_at' => Carbon::now()->subMonth()->format('Y-m-d H:i:s') 
            
        ));

        // 39
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 6,
            'idEjercicio' => 19,
            'dia' => 1,
            'repeticiones' => '45 minutos',
            'created_at' => Carbon::now()->subMonth(5)->format('Y-m-d H:i:s') 
            
        ));

        // 40
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 6,
            'idEjercicio' => 19,
            'dia' => 2,
            'repeticiones' => '45 minutos',
            'created_at' => Carbon::now()->subMonth(5)->format('Y-m-d H:i:s') 
            
        ));

        // 41
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 6,
            'idEjercicio' => 19,
            'dia' => 3,
            'repeticiones' => '45 minutos',
            'created_at' => Carbon::now()->subMonth(5)->format('Y-m-d H:i:s') 
            
        ));

        // 42
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 6,
            'idEjercicio' => 20,
            'dia' => 1,
            'repeticiones' => '3 series de 15 repeticiones',
            'created_at' => Carbon::now()->subMonth(5)->format('Y-m-d H:i:s') 
            
        ));

        // 42
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 6,
            'idEjercicio' => 20,
            'dia' => 2,
            'repeticiones' => '3 series de 15 repeticiones',
            'created_at' => Carbon::now()->subMonth(5)->format('Y-m-d H:i:s') 
            
        ));

        // 42
        DB::table('detalleRutinas')->insert(array(
            'idRutina'=> 6,
            'idEjercicio' => 20,
            'dia' => 3,
            'repeticiones' => '3 series de 15 repeticiones',
            'created_at' => Carbon::now()->subMonth(5)->format('Y-m-d H:i:s') 
            
        ));
        

        
    }
}
