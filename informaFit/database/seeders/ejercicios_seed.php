<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class ejercicios_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // idRutina 	idUsuario 	titulo 	descripcion 	duracion 	link_video 	dificultad 	objetivo 	materiales 	zonaTrabajo 	pdfDieta 	remember_token 	created_at 	updated_at 	
        // 1
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Abdominales',
            'descripcion' => 'Eleva esas carnes',
            'foto' => './assets/img/ejerciciosFotos/abdominales.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
        // 2
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Dominadas',
            'descripcion' => 'Empuja y gana',
            'foto' => './assets/img/ejerciciosFotos/dominadas.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
        // 3
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Flexiones',
            'descripcion' => 'Tu contra la gravedad, quien ganara?',
            'foto' => './assets/img/ejerciciosFotos/flexiones.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
        // 4
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Fondos',
            'descripcion' => 'Esos triceps tontorronees',
            'foto' => './assets/img/ejerciciosFotos/fondos.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
        // 5
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Hiper Extensiones',
            'descripcion' => 'Como la tortura medieval, pero saludable',
            'foto' => './assets/img/ejerciciosFotos/hiperextensiones.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
        // 6
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Plancha Lateral',
            'descripcion' => 'De cara no, imbecil',
            'foto' => './assets/img/ejerciciosFotos/planchaLateral.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
        // 7
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Press banca',
            'descripcion' => 'Si no tienes material en casa, utiliza dos botellas de 10 litros en casa',
            'foto' => './assets/img/ejerciciosFotos/pressBanca.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
        // 8
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Biceps con mancuernas',
            'descripcion' => 'Una mancuerna en cada mano y subir y bajar la mancuerna alternando los dos brazos',
            'foto' => './assets/img/ejerciciosFotos/biceps_mancuernas.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
        // 9
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Curl de biceps',
            'descripcion' => 'Con una barra grande poner peso en cada lado y subir y bajar la barra constantemente',
            'foto' => './assets/img/ejerciciosFotos/curl_biceps.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 10
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Biceps con polea',
            'descripcion' => 'Con la polea abajo cojerla y subir y bajar',
            'foto' => './assets/img/ejerciciosFotos/biceps_polea.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 11
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Extension de piernas en máquina',
            'descripcion' => 'Estira y flexiona las piernas sin parar',
            'foto' => './assets/img/ejerciciosFotos/extension_piernas.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 12
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Prensa inclinada',
            'descripcion' => 'Estira y flexiona las piernas sin parar',
            'foto' => './assets/img/ejerciciosFotos/prensa_inclinada.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 13
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Sentadillas con barra',
            'descripcion' => 'Sentadillas con una barra a la espalda con peso',
            'foto' => './assets/img/ejerciciosFotos/sentadillas_barra.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 14
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Abdominales inferiores',
            'descripcion' => 'Estirate boca arriba y con las piernas estiradas subelas y bajalas',
            'foto' => './assets/img/ejerciciosFotos/abdominales_inferiores.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 15
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Elevación de pelvis',
            'descripcion' => 'Estirate boca arriba y con las piernas flexionadas sube y baja la pelvis',
            'foto' => './assets/img/ejerciciosFotos/elevacion_pelvis.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 16
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Dorsal con mancuerna',
            'descripcion' => 'Con una rodilla en el banco y el otro pie en el suelo estira el brazo y flexionalo subiendo el codo',
            'foto' => './assets/img/ejerciciosFotos/dorsal_mancuerna.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 17
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Remo en polea',
            'descripcion' => 'Tira con todas tus fuerzas',
            'foto' => './assets/img/ejerciciosFotos/remo_polea.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 18
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Polea al pecho',
            'descripcion' => 'Tira con todas tus fuerzas',
            'foto' => './assets/img/ejerciciosFotos/polea_pecho.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 19
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Carrera continua',
            'descripcion' => 'Corre forrest',
            'foto' => './assets/img/ejerciciosFotos/carrera_continua.png',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        // 20
        DB::table('ejercicios')->insert(array(
            'nombre'=> 'Burpies',
            'descripcion' => 'Haz una flexion y salta sucesivamente',
            'foto' => './assets/img/ejerciciosFotos/burpies.jpg',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
    
        
    }
}
