<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class rutinas_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // idRutina 	idUsuario 	titulo 	descripcion 	duracion 	link_video 	dificultad 	objetivo 	materiales 	zonaTrabajo 	pdfDieta 	remember_token 	created_at 	updated_at 	
        // 1
        DB::table('rutinas')->insert(array(
            'idUsuario' => 2,
            'titulo' => 'Rutina fullbody desde casa',
            'descripcion' => 'Rutina para todo el cuerpo pal veranito',
            'duracion' => 1,
            'link_video' => 'https://www.youtube.com/embed/MP_XYEjbKm0',
            'dificultad' => 'facil',
            'objetivo' => 'ganar musculo',
            'materiales' => 'Sin materiales',
            'zonaTrabajo' => 'exterior',
            'pdfDieta' => './assets/docs/guia_nutricion.pdf',
            'created_at' => Carbon::now()->subMonth(3)->format('Y-m-d H:i:s')
        ));
        // 2
        DB::table('rutinas')->insert(array(
            'idUsuario' => 2,
            'titulo' => 'Rutina brazos con mancuernas',
            'descripcion' => 'Rutina para biceps y triceps para estar como un toro...',
            'duracion' => 2,
            'link_video' => 'https://www.youtube.com/embed/lhVKSH2VX3M',
            'dificultad' => 'facil',
            'objetivo' => 'ganar musculo',
            'materiales' => 'Sin materiales',
            'zonaTrabajo' => 'exterior',
            'pdfDieta' => null,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ));
        // 3
        DB::table('rutinas')->insert(array(
            'idUsuario' => 3,
            'titulo' => 'Rutina piernas completa',
            'descripcion' => 'Con esta rutina tendrás las mejores piernas',
            'duracion' => 4,
            'link_video' => null,
            'dificultad' => 'dificil',
            'objetivo' => 'ganar musculo',
            'materiales' => 'Máquinas de gimnasio',
            'zonaTrabajo' => 'gimnasio',
            'pdfDieta' => null,
            'created_at' => Carbon::now()->subYear()->format('Y-m-d H:i:s')
        ));
        // 4
        DB::table('rutinas')->insert(array(
            'idUsuario' => 3,
            'titulo' => 'Rutina sixpack',
            'descripcion' => 'Rutina para definir bien los abdominales',
            'duracion' => 3,
            'link_video' => 'https://www.youtube.com/embed/yhOpmybYE7s',
            'dificultad' => 'facil',
            'objetivo' => 'definición',
            'materiales' => 'Sin materiales',
            'zonaTrabajo' => 'casa',
            'pdfDieta' => null,
            'created_at' => Carbon::now()->subMonth(7)->format('Y-m-d H:i:s')
        ));
        // 5
        DB::table('rutinas')->insert(array(
            'idUsuario' => 4,
            'titulo' => 'Rutina de espalda',
            'descripcion' => 'Rutina para ganar espalda',
            'duracion' => 5,
            'link_video' => null,
            'dificultad' => 'intermedio',
            'objetivo' => 'ganar musculo',
            'materiales' => 'Máquinas de gimnasio',
            'zonaTrabajo' => 'gimnasio',
            'pdfDieta' => null,
            'created_at' => Carbon::now()->subMonth()->format('Y-m-d H:i:s')
        ));
        // 6
        DB::table('rutinas')->insert(array(
            'idUsuario' => 4,
            'titulo' => 'Rutina para perder peso',
            'descripcion' => 'Con esta rutina notarás resultados en tan solo 2 meses',
            'duracion' => 2,
            'link_video' => 'https://www.youtube.com/embed/WnoCFnIiQHw',
            'dificultad' => 'facil',
            'objetivo' => 'perder peso',
            'materiales' => 'Sin materiales',
            'zonaTrabajo' => 'exterior',
            'pdfDieta' => './assets/docs/rutina_cardio.pdf',
            'created_at' => Carbon::now()->subMonth(5)->format('Y-m-d H:i:s')
        ));
    }
}
