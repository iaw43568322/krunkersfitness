<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class rutinasxusuarios_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // idRutina 	idUsuario 	titulo 	descripcion 	duracion 	link_video 	dificultad 	objetivo 	materiales 	zonaTrabajo 	pdfDieta 	remember_token 	created_at 	updated_at 	
        DB::table('rutinasxusuarios')->insert(array(
            'idRutina' => 1,
            'idUsuario' => 1,
            'diasTotales' => 20,
            'diasCompletados'=> 6,
            'progreso' => 30,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));

        DB::table('rutinasxusuarios')->insert(array(
            'idRutina' => 2,
            'idUsuario' => 1,
            'diasTotales' => 24,
            'diasCompletados'=> 0,
            'progreso' => 0,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s') 
            
        ));
    
        
    }
}
