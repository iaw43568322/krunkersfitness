<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class usuarios_seed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // idUsuario 	nombre 	apellidos 	dni 	email 	contrasena 	telefono 	tipo 	remember_token 	created_at 	updated_at 	
        // 1
        DB::table('usuarios')->insert(array(
            'nombre' => 'Jacint',
            'apellidos' => 'Iglesias Martinez',
            'descripcion' => 'Me encanta el deporte y la cerveza',
            'dni' => '12345678R',
            'email' => 'jacint@jacint.es',
            'contrasena' => 'holaquetal',
            'telefono' => '613125433',
            'tipo' => 'cliente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'fotoUsuario' => '../assets/img/fotoUsuario/usuario1.jpg'
        ));
        // 2
        DB::table('usuarios')->insert(array(
            'nombre' => 'Joshtyn',
            'apellidos' => 'Loma Mamadismo',
            'descripcion' => 'Me gustaria estar como Silvester Stallone',
            'dni' => '51432475R',
            'email' => 'prueba@prueba.es',
            'contrasena' => 'holaquetal',
            'telefono' => '642143243',
            'tipo' => 'entrenador',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'fotoUsuario' => '../assets/img/fotoUsuario/usuario2.jpg'
        ));
        // 3
        DB::table('usuarios')->insert(array(
            'nombre' => 'Erik',
            'apellidos' => 'Perdigones Garcia',
            'descripcion' => 'Entrenador para fuertes',
            'dni' => '87654321Z',
            'email' => 'erik@erik.es',
            'contrasena' => 'holaquetal',
            'telefono' => '613125422',
            'tipo' => 'entrenador',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'fotoUsuario' => '../assets/img/fotoUsuario/usuario2.jpg'
        ));
        // 4
        DB::table('usuarios')->insert(array(
            'nombre' => 'Alex',
            'apellidos' => 'Francisco Tinaya',
            'descripcion' => 'Solo entreno a bestias',
            'dni' => '13579246A',
            'email' => 'alex@alex.es',
            'contrasena' => 'holaquetal',
            'telefono' => '613125411',
            'tipo' => 'entrenador',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'fotoUsuario' => '../assets/img/fotoUsuario/usuario2.jpg'
        ));
        // 5
        DB::table('usuarios')->insert(array(
            'nombre' => 'Ignasi',
            'apellidos' => 'Brugada Andueza',
            'descripcion' => 'Quiero ponerme mazado',
            'dni' => '24681357I',
            'email' => 'ignasi@ignasi.es',
            'contrasena' => 'holaquetal',
            'telefono' => '613125444',
            'tipo' => 'cliente',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'fotoUsuario' => '../assets/img/fotoUsuario/usuario1.jpg'
        ));
    }
}
