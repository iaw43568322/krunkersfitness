"use strict";

// Declaració de variables
var btnEditar = document.getElementById("btnEditar");
btnEditar.addEventListener("click", editarDatosAcceso);

/**
 * Funció que mostra un formulari per
 * editar les dades d'accés
 */
function editarDatosAcceso() {
    let divDatos = document.getElementById("divDatos");
    let formDatos = document.getElementById("formDatos");

    // Canviem la propietat display
    divDatos.style.display = "none";
    formDatos.style.display = "block";
}