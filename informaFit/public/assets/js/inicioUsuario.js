"use strict";

// Declaració de variables
var btnFilters = document.getElementById("btnFilters");
btnFilters.addEventListener("click", openModal);

var modal = document.getElementById("modal");

var span = document.getElementById("close");
span.addEventListener("click", closeModal);

// Funció que obre el modal
function openModal() {
    modal.style.display = "block";
}

// Funció que tanca el modal
function closeModal() {
    modal.style.display = "none";
}

// Si l'usuari clica fora del modal tanquem el modal
window.onclick = function() {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}