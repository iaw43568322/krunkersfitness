@extends('layouts.cliente')
@section('title', 'Darse de baja Cliente - informafit')

@section('content')

<div class="top-bar d-none d-md-block" style="margin-top: 8%;">
    <div class="container-fluid">
        <div class="row">

        </div>
    </div>
</div>

<!-- Form start -->
<div class="container p-5">
    <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12 bg-light text-dark p-3 rounded">
            <form action="/bajaUsuario" method="post" onsubmit="return confirm('¿Realmente quieres dejar de usar nuestro servicio? Al darse de baja seras redirigido a la pagina principal');">
                {{ csrf_field() }}
                @if (session('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>
                @endif
                <h3>Por qué quieres darte de baja?</h3>
                <input type="checkbox" id="1">
                <label for="1" class="text-dark">Pensaba que el contenido de la página era otro</label>
                <br><br>
                <input type="checkbox" id="2">
                <label for="2" class="text-dark">No me gusta el contenido</label>
                <br><br>
                <input type="checkbox" id="3">
                <label for="3" class="text-dark">No tengo tiempo para entrenar</label>
                <br><br>
                <input type="checkbox" id="4">
                <label for="4" class="text-dark">No me gusta el formato de las rutinas</label>
                <br><br>
                <input type="checkbox" id="5">
                <label for="5" class="text-dark">Otros</label>
                <br><br>
                <label for="comentarios" class="text-dark">¿Qué otros motivos te llevan a darte de baja? Comentarios:</label>
                <br>
                <textarea name="comentarios" id="comentarios" cols="auto" rows="10"></textarea>
                <br><br>
                @error('password')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <i class="fas fa-unlock-alt"></i>
                <label for="password" class="text-dark">Contraseña</label>
                <br>
                <input type="password" name="password" id="contraseña">
                <br><br>
                <i class="fas fa-unlock-alt"></i>
                <label for="passwordVerify" class="text-dark">Repetir Contraseña</label>
                <br>
                <input type="password" name="passwordVerify" id="passwordVerify">
                <br><br>
                <input type="submit" class="btn btn-danger" value="Darse de baja">
                
            </form>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12">
            <div class="mb-5 mt-4" style="background-image: url('.././assets/img/fondoRayadoRojo.png'); background-size: cover; background-position: center;">
                <h1 class="text-light">Información</h1>
                <p class="text-light" style="font-weight: bold">Al darte de baja de InformaFit, todos tus datos de carácter personal serán borrados de nuestras bases de datos. También serán borradas todas tus rutinas actuales. Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus eligendi, cum non placeat voluptatum tempore distinctio illum quam culpa? Iusto expedita amet veritatis a? Porro eius, consequatur suscipit quia nam, quo amet aliquid error est nihil deleniti maiores aut expedita! Dignissimos, ducimus rerum magnam id amet doloremque eius animi quis?</p>
            </div>
        </div>
    </div>
</div>
<!-- Form end -->

<!-- Scripts -->
<script src=".././assets/js/ajustes.js"></script>

@endsection