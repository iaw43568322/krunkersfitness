@extends('layouts.cliente')
@section('title', 'Descripción rutina - informafit')

@section('content')
<div class="container text-light mt-5">
    @forelse($rutinas as $rutina)
        <div>
            @switch($rutina->objetivo)
                @case("ganar musculo")
                    <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/bradcam_2.png') }}); background-size: cover">
                    @break
                @case("definicion")
                    <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/bradcam_1.png') }}); background-size: cover">
                    @break
                @case("perder peso")
                    <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/banner2.png') }}); background-size: cover">
                    @break
                @default
                    <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/big_offer.png') }}); background-size: cover">
            @endswitch
            
            </div>
            <div style="border: 1px solid black; display: inline-block; max-width: 50%; padding: 10px; word-break: break-all; margin-top: -20px; margin-left: 10px;  background-color: white;">
                <p class="text-center" style="color:black; font-size: 30px;">{{  $rutina->titulo  }}</p>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-lg-8">
                <h2 class="text-light">Descripción:</h2>
                <p class="text-light">{{ $rutina->descripcion }}</p>
                <h2 class="text-light mt-3">Dieta:</h2>
                @if($rutina->pdfDieta !== null)
                <embed src="{{ asset( $rutina->pdfDieta ) }}" type="application/pdf" width="100%" height="600px" />
                @else
                <p>No hay ninguna dieta descrita</p>
                @endif
                <h2 class="text-light mt-3">Video rutina:</h2>
                @if($rutina->link_video !== null)
                <div class="video-responsive">    
                    <iframe class="mt-5" width="560" height="315" src="{{ $rutina->link_video }}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                @else
                    <p>No hay video disponible</p>
                @endif
                <h3 class="text-light mt-3">Ejercicios:</h3>
                
                    <div class="accordion mt-3 mb-3" id="accordionExample">
                        <div class="card">
                            @forelse($dias as $dia)
                                <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $dia->dia }}" aria-expanded="true" aria-controls="collapseOne">
                                        Dia {{ $dia->dia }}
                                    </button>
                                </h2>
                                </div>

                                <div id="collapse{{ $dia->dia }}" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <ul class="text-dark">
                                            @forelse($ejercicios as $ejercicio)
                                                <!-- Miramos si el ejercicio pertenece al dia que toca -->
                                                @if( $ejercicio->dia == $dia->dia )
                                                    <li class="border-bottom d-flex justify-content-around align-items-center">
                                                        <span>
                                                            <h3>{{ $ejercicio->nombre }}</h4>
                                                            {{ $ejercicio->repeticiones }}
                                                            <br>
                                                            {{ $ejercicio->descripcion }}
                                                        </span>
                                                        <img width="50%" src="{{ asset( $ejercicio->foto ) }}" alt="fondos">
                                                    </li>
                                                @endif
                                            @empty
                                                <p>No hay ejercicios para este dia</p>
                                            @endforelse
                                        </ul>
                                    </div>
                                </div>
                            @empty
                                <p>No hay ejercicios</p>
                            @endforelse
                        </div>
                    </div>
                
            </div>
            <div class="col-lg-4">
                <!-- Recorremos las rutinas a las que está suscrito para poner un boton u otro -->
                @if(array_search($rutina->idRutina, $idRutinasSuscritas) === false)
                    <form action="suscripcionRutina" method="POST">
                        {{ csrf_field() }}
                        <input type="hidden" name="idRutina" value="{{$rutina->idRutina}}">
                        <input class="btn btn-warning btn-block" type="submit" value="Suscribirse">
                    </form>
                @else
                    <div id="divProgreso">
                    @foreach($rutinasSuscritas as $rutinaSuscrita)
                        @if($rutina->idRutina == $rutinaSuscrita->idRutina)
                            @if($rutinaSuscrita->diasCompletados < $rutinaSuscrita->diasTotales)
                                <form action="añadirDiaCompletado" method="POST" onsubmit="return confirm('¿Has completado el dia? No vale hacer trampas');">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="idRutina" value="{{$rutina->idRutina}}">
                                    <input class="btn btn-success btn-block" type="submit" value="Completar dia {{ $rutinaSuscrita->diasCompletados + 1 }}">
                                </form>
                            @else
                                <form action="añadirDiaCompletado" method="POST" onsubmit="return confirm('Enhorabuena por completar la rutina! ¿Quieres empezar de 0?');">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="idRutina" value="{{$rutina->idRutina}}">
                                    <input class="btn btn-success btn-block" type="submit" value="Reiniciar progreso">
                                </form>
                            @endif
                            <br>
                            <h4 class="text-light">Progreso: </h4>
                            <p>
                                Días completados: {{ $rutinaSuscrita->diasCompletados }} dias
                                <br>
                                Días por completar: {{ $rutinaSuscrita->diasTotales - $rutinaSuscrita->diasCompletados }} dias
                                <br>
                                Días Totales: {{ $rutinaSuscrita->diasTotales }} dias
                                <br>
                                <label for="progreso" class="text-light">Progreso: {{round($rutinaSuscrita->progreso, 2)}} %</label><br>
                                <progress id="progreso" max="100" value="{{$rutinaSuscrita->progreso}}"></progress>
                            </p>
                        @endif
                    @endforeach
                    </div>
                    
                    <br>
                    <form action="eliminarSuscripcion" method="POST" onsubmit="return confirm('¿Quieres eliminar esta rutina de tus suscripciones?');">
                        {{ csrf_field() }}
                        <input type="hidden" name="idRutina" value="{{$rutina->idRutina}}">
                        <input class="btn btn-danger btn-block" type="submit" value="Borrar suscripción">
                    </form>
                @endif

                <div class="border bg-light p-3 mt-4 border rounded" style="color: black;">
                    <h4>Objetivo:</h4>
                    <p>{{ $rutina->objetivo }}</p>
                    <h4>Duración:</h4>
                    <p>{{ $rutina->duracion }} mes/es</p>
                    <h4>Dificultad:</h4>
                    <p>{{ $rutina->dificultad }}</p>
                    <h4>Materiales:</h4>
                    <p>{{ $rutina->materiales }}</p>
                    <h4>Zona de trabajo:</h4>
                    <p>{{ $rutina->zonaTrabajo }}</p>
                    <h4>Valoración:</h4>
                    <p>5 estrellas</p>
                </div>
            </div>
        </div>
    @empty
        <p>No se ha encontrado información sobre la rutina seleccionada</p>
    @endforelse
</div>

@endsection