@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.cliente')
@section('title', 'Inicio - informafit')
@section('css')
    <link rel="stylesheet" href="./assets/css/inicioUsuario.css" />
@endsection

@section('content')
@if(!Request::wantsJson())
<!-- <div class="top-bar d-none d-md-block" style="margin-top: 5%;">
    <div class="container-fluid">
        <div class="row">

        </div>
    </div>
</div> -->

<div class="container">
    <h1 class="text-white mt-5">Rutinas más visitadas</h1>
    <div class="row">
        @include('layouts.filters')
        @endif
        <!-- Rutinas Start -->
        @if(!Request::wantsJson())
        <div class="col-lg-8 d-flex flex-wrap order-lg-1 order-sm-2">
            @endif
            <div id="rutinaList">
                @include('layouts.rutinaList')
            </div>
        </div>
        <!-- Rutinas End -->
    </div>
</div>

<!-- Scripts -->
<script src=".././assets/js/inicioUsuario.js"></script>

@endsection
