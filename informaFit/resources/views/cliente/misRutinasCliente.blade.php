@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.cliente')
@section('title', 'Mis suscripciones - informafit')
@section('css')
    <link rel="stylesheet" href="./assets/css/inicioUsuario.css" />
@endsection

@section('content')
@if(!Request::wantsJson())

<div class="container">
    <h1 class="text-white mt-5">Mis suscripciones</h1>
    @if(app('request')->has('success'))
        @if(app('request')->input('success'))
        <div class="alert alert-success">
            Rutina eliminada correctamente
        </div>
        @endif
    @endif
    @if(app('request')->has('successDelete'))
        @if(app('request')->input('successDelete'))
        <div class="alert alert-success">
            Todas las rutinas han sido eliminadas correctamente
        </div>
        @endif
    @endif
    <div class="row">
        @include('layouts.filters')
        @endif
        <!-- Rutinas Start -->
        @if(!Request::wantsJson())
        <div class="col-lg-8 d-flex flex-wrap order-lg-1 order-sm-2">
            @endif
            <div id="rutinaList">
                @include('layouts.suscripcionesList')
            </div>
        </div>
        <!-- Rutinas End -->
    </div>
</div>

<!-- Scripts -->
<script src=".././assets/js/inicioUsuario.js"></script>

@endsection
