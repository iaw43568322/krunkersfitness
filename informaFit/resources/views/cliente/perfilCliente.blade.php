@extends('layouts.cliente')
@section('title', 'Perfil cliente - informafit')

@section('content')
<div class="container text-light mt-5">
    <h1 class="text-light">Mi perfil</h1>
    <div>
        <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/offer.png') }}); background-size: cover">
        </div>
        <p class="text-center" style="margin-top: -20px;"><img class="rounded-circle" width="125px" src="{{ $data[0]->fotoUsuario }}" alt="foto_cliente"></p>
    </div>
    <h2 class="text-center text-light mt-3">{{ $data[0]->nombre }} {{ $data[0]->apellidos }}</h2>
    <div class="mb-3">
        <h3 class="text-light">Sobre mi:</h3>
        <p class="text-light">{{ $data[0]->descripcion }}</p>
    </div>
    <div class="d-flex justify-content-center">
        <a href="{{ url('/cliente/ajustes') }}" class="boxed-btn3">Actualizar perfil</a>
        <a href="{{ url('/cliente/darseBaja') }}" class="boxed-btn3 ml-3">Darse de baja</a>
    </div>
</div>
@endsection