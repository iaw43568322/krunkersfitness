@extends('layouts.general')
@section('title', 'Registro Cliente - Informafit')

@section('content')
<div class="container text-light mt-5">
    <h1 class="text-light">Registro Cliente</h1>
    @if(app('request')->has('success'))
        @if(app('request')->input('success'))
        <div class="alert alert-success">
            Usuario registrado correctamente
        </div>
        @endif
    @endif
    @if (session('error'))
        <div class="alert alert-danger">{{ session('error') }}</div>
    @endif


    <form method="post" action={{route('usuarios.addCliente')}} enctype="multipart/form-data">
        {{ csrf_field() }} 
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                @error('nombre')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">
                            <label for="nombre">
                            Nombre
                            </label>
                        </span>
                    </div>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="nombre" placeholder="Juan" value="{{ old('nombre') }}">
                </div>

                @error('apellidos')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1">
                                <label for="apellidos">Apellidos</label>
                            </span>
                    </div>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" name="apellidos" placeholder="Lopez Jimenez" value="{{ old('apellidos') }}">
                </div>
                
                @error('email')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <label for="email" class="text-light">Correo Electronico</label>
                <div class="input-group mb-3">
                    <input id="email" type="text" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="juan.lopez@ejemplo.com" value="{{ old('email') }}">
                </div>

                <label for="fotoUsuario" class="text-light">Foto perfil: </label><br>
                <input type="file" name="fotoUsuario">
                <br><br>

                @error('password')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <label for="contrasena" class="text-light">Contraseña</label>
                <div class="input-group mb-3">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">
                </div>

                @error('passwordAgain')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <label for="passwordAgain" class="text-light">Repetir Contraseña</label>
                <div class="input-group mb-3">
                    <input id="passwordAgain" type="password" class="form-control @error('passwordAgain') is-invalid @enderror" name="passwordAgain">
                </div>
            </div>
            <div class="col-lg-6 col-sm-12" style="background-image: url('./assets/img/fondoRayadoRojo.png'); background-size: cover; background-position: center;">
                <h3 class="text-light">Ventajas de ser cliente</h3>
                <p class="text-light" style="font-weight: bold">Recently, the US Federal government banned online casinos from operating in America by making it illegal to transfer money to them through any US bank or payment system. As a result of this law, most of the popular online casino networks such as Party Gaming and PlayTech left the United States. Overnight, online casino players found themselves being chased by the Federal government. But, after a fortnight, the online casino industry came up with a solution and new online casinos started taking root. These began to operate under a different business umbrella, and by doing that, rendered the transfer of money to and from them legal.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
            <br><br>
                @error('descripcion')
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Hablanos sobre ti</span>
                    </div>
                    <textarea class="form-control" name="descripcion" value="{{ old('descripcion') }}"></textarea>
                </div>
                <br>
                <input type="hidden" name="tipo" value="cliente">
                <button type="submit" class="genric-btn primary circle">Registrarse</button>
            </div>
        </div>
    </form>

    <br>
    <p>¿Deseas registrarte como entrenador? Pulsa <a class="text-light" href="{{ url('/registroEntrenador') }}">aqui</a></p>
</div>
@endsection