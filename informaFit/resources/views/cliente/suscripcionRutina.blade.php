@extends('layouts.cliente')
@section('title', 'Suscripcion Rutina - Informafit')

@section('content')
<div class="container text-light mt-5">
    <h1 class="text-light">Suscripcion Rutina</h1>
    <div>
        @switch($rutina[0]->objetivo)
            @case("ganar musculo")
                <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/bradcam_2.png') }}); background-size: cover">
                @break
            @case("definicion")
                <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/bradcam_1.png') }}); background-size: cover">
                @break
            @case("perder peso")
                <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/banner2.png') }}); background-size: cover">
                @break
            @default
                <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/big_offer.png') }}); background-size: cover">
        @endswitch
        
        </div>
        <div style="border: 1px solid black; display: inline-block; max-width: 50%; padding: 10px; word-break: break-all; margin-top: -20px; margin-left: 10px;  background-color: white;">
            <p class="text-center" style="color:black; font-size: 30px;">{{ $rutina[0]->titulo }}</p>
        </div>
    </div>
    <br><br><br>
    <div style="border: 1px solid white; border-radius: 20px 5px 20px 5px; display: inline-grid; width: 100%;">
        <h6 class="" style="color:white; margin-top: -10px; display: inline-block; margin-left: 120px; background: black; max-width: 15.5%; padding-left:5px;">Formulario Suscripción</h6>
        <div class="row">
            <div class="col-lg-6 col-sm-12" style="padding-bottom:50px;">
                <p style="color:white; margin-left: 10px; padding-bottom:20px;">Quieres vincular tu rutina con Google Calendar?</p>
                <button class="genric-btn success circle small" style="margin-left: 20px;">Permitir</button>
                <button class="genric-btn primary circle small" style="margin-left: 50px;">Denegar</button>
            </div>
        </div>
        
                <p style="color:white; margin-left: 10px; padding-bottom:20px;">Que dias quieres entrenar y en que franja horaria?</p>
                <div class="table-responsive">
                    <table class="table" style="">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Lunes</th>
                                <th scope="col">Martes</th>
                                <th scope="col">Miercoles</th>
                                <th scope="col">Jueves</th>
                                <th scope="col">Viernes</th>
                                <th scope="col">Sabado</th>
                                <th scope="col">Domingo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">Mañana</th>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                            </tr>
                            <tr>
                                <th scope="row">Tarde</th>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                            </tr>
                            <tr>
                                <th scope="row">Noche</th>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                                <td class="text-center"><input type="checkbox"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
        <div class="row">
            <div class="col-lg-12 col-sm-12" style="padding-bottom:10px;">

                <input class="genric-btn info circle e-large" type="submit" style="margin-right: 50px; float: right;" value="Suscribirse">
            </div>
        </div>
    </div>

</div>
@endsection
