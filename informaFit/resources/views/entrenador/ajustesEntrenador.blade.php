@extends('layouts.entrenador')
@section('title', 'Ajustes Entrenador - informafit')

@section('content')

<div style="background-image: url('.././assets/img/banner/pesas2.jpg'); background-size: cover;">

<!-- Form start -->
<div class="container mt-lg-3 mt-sm-5 mt-xs-5 p-5">
    <div class="row">
        <div class="col align-self-center" style="width: 80%; display: flex; align-items: center; justify-content: center;">
            <div id="divDatos" class="bg-light text-dark p-5 rounded" style="display: block;">
                <h3>Ajustes</h3>
                <hr>
                <h4>Modificar contraseña</h4>
                @if(app('request')->has('success'))
                    @if(app('request')->input('success'))
                    <div class="alert alert-success">
                        Contraseña modificada correctamente
                    </div>
                    @endif
                @endif
                <form action="/modificarPassword" method="POST">
                {{ csrf_field() }}
                @if (session('error'))
                    <span class="text-danger">
                        <strong>{{session('error')}}</strong>
                    </span>
                    <br>
                @endif
                @error('passwordActual')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <i class="fas fa-unlock-alt"></i>
                <label for="password" class="text-dark">Contraseña actual</label>
                <br>
                <input class="form-control @error('name') is-invalid @enderror" type="password" name="passwordActual" id="password" placeholder="********">
                @error('password')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <i class="fas fa-unlock-alt"></i>
                <label for="password" class="text-dark">Nueva contraseña</label>
                <br>
                <input class="form-control @error('name') is-invalid @enderror" type="password" name="password" id="password" placeholder="********">
                <i class="fas fa-unlock-alt"></i>
                <label for="passwordVerify" class="text-dark">Verificar contraseña</label>
                <br>
                <input class="form-control" type="password" name="passwordVerify" id="passwordVerify" placeholder="********">
                <br><br>
                <!-- <input type="submit" id="btnEditar" class="btn btn-primary" value="Editar datos de acceso"> -->
                <input type="submit" class="btn btn-block btn-primary" value="Editar datos de acceso">
                </form>
                <hr>
                <button id="btnBaja" class="btn btn-block btn-danger"><a href="/entrenador/darseBaja" class="text-light">Darse de baja</a></button>
            </div> 


            <!-- Formulario que se activa si quiere editar los datos -->
            <!-- <form id="formDatos" action="#" class="bg-light text-dark p-5 rounded" style="display: none;">
                <h3>Datos de acceso</h3>
                <i class="fas fa-envelope"></i>
                <label for="correoElectronico" class="text-dark">Correo electrónico</label>
                <br>
                <input type="text" name="correoElectronico" id="correoElectronico" placeholder="example@gmail.com">
                <br><br>
                <i class="fas fa-unlock-alt"></i>
                <label for="contraseña" class="text-dark">Contraseña</label>
                <br>
                <input type="password" name="contraseña" id="contraseña" placeholder="********">
                <br><br>
                <input type="submit" class="btn btn-success" value="Editar">
                <button class="btn btn-danger ml-5">Cancelar</button>
            </form> -->
        </div>
    </div>
</div>
<!-- Form end -->

</div>
<!-- Scripts -->

@endsection