@extends('layouts.entrenador')
@section('title', 'Crear nueva rutina - informafit')

@section('content')
<div class="container" style="margin-top: 3%;">
    <h1 class="text-light">Crear nueva rutina</h1>

    <div class=" p-3">
        <form id="crearRutina" action="crearRutina" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
            @error('title')
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            <label for="title">Titulo rutina:</label> <br>
            <input type="text" id="title" name="title" placeholder="Rutina fullbody para definir..." value="{{ old('title') }}"> <br>

            @error('description')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            <label for="description">Descripción:</label> <br>
            <textarea id="description" name="description" cols="30" rows="10" placeholder="Rutina fullbody bla bla..."></textarea> <br>

            @error('duration')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            <label for="duration">Duración (en meses):</label> <br>
            <input type="text" id="duration" name="duration" value="{{ old('duration') }}"> <br>

            @error('difficulty')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            <label for="difficulty">Dificultad:</label> <br>
            <input type="radio" id="facil" name="difficulty" value="facil">
            <label for="facil">Fácil</label>
            <input type="radio" id="intermedio" name="difficulty" value="intermedio">
            <label for="intermedio">Intermedio</label>
            <input type="radio" id="dificil" name="difficulty" value="dificil">
            <label for="dificil">Difícil</label> 

            <br>

            <h3 class="text-light mt-3">Ejercicios:</h3>
            @if (session('error'))
                <div class="alert alert-danger">{{ session('error') }}</div>
            @endif
            @error('nameExercise.*')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            @error('descripcionEjercicio.*')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            @error('repeticiones.*')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            @error('dias.*')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            <div id="exercices" class="mt-4">
                <!-- Week div -->
                <div class='table-responsive'>
                    <table class="table table-bordered" id="tableExercises">
                        <tr>
                            <th>Nombre:</th>
                            <th>Descripcion:</th>
                            <th>Repeticiones/duracion:</th>
                            <th>Foto:</th>
                            <th>Dia/s (1,2,4):</th>
                            <th><input type="button" class="btn btn-success" id="addExercise" value="Añadir ejercicio"></th>
                        </tr> 
                        <tr>
                            <td>
                                <!-- <label for="nameExercise[]">Nombre:</label> <br> -->
                                <input type="text" name="nameExercise[]" id="nameExercise" >
                            </td>
                            <td>
                                <input type="text" id="descripcionEjercicio" name="descripcionEjercicio[]">
                            </td>
                            <td>
                                <input type="text" id="repeticiones" name="repeticiones[]">
                            </td>
                            <td>
                                <input type="file" name="foto[]" id="foto">
                            </td>
                            <td>
                                <input type="text" name="dias[]" id="dias">
                            </td>
                            <td>
                                <input type="button" id="removeExercise" value="Borrar">
                            </td>
                        </tr>  
                    </table>
                </div>
            </div>

            @error('material')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            <label for="material">Materiales:</label> <br>
            <input type="text" name="material" id="material" placeholder="mancuernas, goma elástica..." value="{{ old('material') }}"> <br>

            @error('zone')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            <label for="zone">Zona de trabajo:</label> <br>
            <select name="zone" id="zone" class="text-dark">
                <option value="gimnasio">Gimnasio</option> 
                <option value="exterior">Exterior</option>      
                <option value="casa">Casa</option>       
            </select>
             <br>
            <br>

            @error('objetive')
            <br>
                <span class="text-danger">
                    <strong>{{ $message }}</strong>
                </span>
                <br>
            @enderror
            <label for="objective">Objetivo:</label> <br>
            <select name="objective" id="objective" class="text-dark">
                <option value="perder peso">Perder peso</option> 
                <option value="definicion">Definición</option>      
                <option value="ganar musculo">Ganar músculo</option>       
            </select>

            <br>
            <br>
            <label for="link">Link video (opcional):</label><br>
            <input type="text" id="link" name="link" placeholder="https://www.youtube.com/" value="{{ old('link') }}">

            <br>
            <label for="diet">Dieta (opcional):</label> <br>
            <input type="file" name="diet" id="diet">

            <br>
            <br>
            <input type="submit" value="Guardar">
        </form>
    </div>
</div>
@endsection

@section('script_tablas')
<script type="text/javascript">
$(document).ready(function() {
    // Add exercise
    var contExercise = 1;
    $("#addExercise").click(function() {
        contExercise++;
        var htmlExercise = `<tr>
                            <td>
                                <input type="text" name="nameExercise[]" id="nameExercise">
                            </td>
                            <td>
                                <input type="text" name="descripcionEjercicio[]" id="descripcionEjercicio">
                            </td>
                            <td>
                                <input type="text" name="repeticiones[]" id="repeticiones">
                            </td>
                            <td>
                                <input type="file" name="foto[]" id="foto">
                            </td>
                            <td>
                                <input type="text" name="dias[]" id="dias">
                            </td>
                            <td>
                                <input type="button" id="removeExercise" value="Borrar">
                            </td>
                        </tr>  `;
        $("#tableExercises").append(htmlExercise);
    });

    // Remove exercise
    $("#tableExercises").on('click', '#removeExercise', function() {
        $(this).closest('tr').remove();
        contExercise--;
    });

    
});
</script>
@stop