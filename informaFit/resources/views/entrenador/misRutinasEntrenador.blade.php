@extends((Request::wantsJson()) ? 'layouts.ajax' : 'layouts.entrenador')
@section('title', 'Mis rutinas - informafit')
@section('css')
    <link rel="stylesheet" href=".././assets/css/inicioUsuario.css" />
@endsection

@section('content')

<!-- <div class="top-bar d-none d-md-block" style="margin-top: 5%;">
    <div class="container-fluid">
        <div class="row">

        </div>
    </div>
</div> -->
@if(!Request::wantsJson())
<div class="container">
    @if(app('request')->has('success'))
        @if(app('request')->input('success'))
        <div class="alert alert-success">
            Rutina eliminada correctamente
        </div>
        @endif
    @endif
    @if(app('request')->has('successDelete'))
        @if(app('request')->input('successDelete'))
        <div class="alert alert-success">
            Todas las rutinas han sido eliminadas correctamente
        </div>
        @endif
    @endif
    
    <h1 class="text-white mt-5">Mis rutinas</h1>
    <div class="row">
        <!-- Filtros Start -->
        @include('layouts.filtersEntrenador')
        @endif
        <!-- Filtros End -->
        <!-- Rutinas Start -->
        @if(!Request::wantsJson())
        <div class="col-lg-8 d-flex flex-wrap order-lg-1 order-sm-2">
        @endif
            <div id="rutinaList">
            @include('layouts.rutinaListEntrenador')
            </div>
        </div>
        <!-- Rutinas End -->
    </div>
</div>

<!-- Scripts -->
<script src=".././assets/js/inicioUsuario.js"></script>


@endsection
