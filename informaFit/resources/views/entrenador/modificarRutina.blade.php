@extends('layouts.entrenador')
@section('title', 'Modificar rutina - informafit')

@section('content')
<div class="container" style="margin-top: 3%;">
    <h1 class="text-light">Modificar rutina</h1>

    <div class=" p-3">
        <form action="modificarRutina" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input type="hidden" name="idRutina" id="idrutina" value="{{$rutina[0]->idRutina}}">
            <label for="title">Titulo rutina:</label> <br>
            <input type="text" name="title" placeholder="Rutina fullbody para definir..." value="{{$rutina[0]->titulo}}"> <br>

            <label for="description">Descripción:</label> <br>
            <textarea name="description" cols="30" rows="10" placeholder="Rutina fullbody bla bla...">{{ $rutina[0]->descripcion }}</textarea> <br>

            <label for="duration">Duración (en meses):</label> <br>
            <input type="text" name="duration" value="{{$rutina[0]->duracion}}"> <br>

            <label for="difficulty">Dificultad:</label> <br>
            <input type="radio" id="facil" name="difficulty" value="facil" @if($rutina[0]->dificultad == 'facil') checked @endif>
            <label for="facil">Fácil</label>
            <input type="radio" id="intermedio" name="difficulty" value="intermedio" @if($rutina[0]->dificultad == 'intermedio') checked @endif>
            <label for="intermedio">Intermedio</label>
            <input type="radio" id="dificil" name="difficulty" value="dificil" @if($rutina[0]->dificultad == 'dificil') checked @endif>
            <label for="dificil">Difícil</label> 

            <br>
            <h3 class="text-light mt-3">Ejercicios:</h3>
            <div id="exercices" class="mt-4">
                <!-- Week div -->
                <div class='table-responsive'>
                    <table class="table table-bordered" id="tableExercises">
                        <tr>
                            <th style="display: none;"></th>
                            <th>Nombre:</th>
                            <th>Descripcion:</th>
                            <th>Repeticiones/duracion:</th>
                            <th>Foto actual ejercicio</th>
                            <th>Cambiar foto ejercicio:</th>
                            <th>Dia/s (1,2,4):</th>
                            <th><input type="button" class="btn btn-success" id="addExercise" value="Añadir ejercicio"></th>
                        </tr>
                            @foreach($ejercicios as $ejercicio)
                            <input type="hidden" name="idEjercicioActuales[]" value="{{$ejercicio->idEjercicio}}" >
                        <tr>
                            <td style="display:none;">
                            <input type="hidden" name="idEjercicio[]" value="{{$ejercicio->idEjercicio}}" >
                            </td>
                            <td>
                                <!-- <label for="nameExercise[]">Nombre:</label> <br> -->
                                <input type="text" name="nameExercise[]" id="nameExercise" value="{{$ejercicio->nombre}}">
                            </td>
                            <td>
                                <input type="text" id="descripcionEjercicio" name="descripcionEjercicio[]" value="{{$ejercicio->descripcion}}">
                            </td>
                            <td>
                                <input type="text" id="repeticiones" name="repeticiones[]" value="{{$ejercicio->repeticiones}}">
                            </td>
                            <td>
                                <img width="100%" height="100%" src="{{ asset( $ejercicio->foto ) }}" alt="foto_ejercicio">
                            </td>
                            <td>
                                <input type="file" name="foto[]" id="foto">
                            </td>
                            <td>
                            <?php
                                $diasRutina = '';
                                foreach($dias as $dia) {
                                    if($dia->idEjercicio == $ejercicio->idEjercicio) {
                                        $diasRutina .= $dia->dia.',';
                                    }
                                }
                                $diasRutina = substr_replace($diasRutina, "", -1);
                            ?>
                            <input type="text" name="dias[]" id="dias" value="{{$diasRutina}}">
                            </td>
                            <td>
                                <input type="button" id="removeExercise" value="Borrar">
                            </td>
                        </tr>  
                        @endforeach
                    </table>
                </div>
            </div>

            <br>

            <label for="material">Materiales:</label> <br>
            <input type="text" name="material" placeholder="mancuernas, goma elástica..." value="{{$rutina[0]->materiales}}"> <br>

            <label for="zone">Zona de trabajo:</label> <br>
            <select name="zone" id="zone" class="text-dark">
                <option value="gimnasio"  @if($rutina[0]->zonaTrabajo == 'gimnasio') selected @endif>Gimnasio</option> 
                <option value="exterior" @if($rutina[0]->zonaTrabajo == 'exterior') selected @endif>Exterior</option>      
                <option value="casa" @if($rutina[0]->zonaTrabajo == 'casa') selected @endif>Casa</option>       
            </select>
             <br>
            <br>
            <label for="objective">Objetivo:</label> <br>
            <select name="objective" id="objective" class="text-dark">
                <option value="perder peso" @if($rutina[0]->objetivo == 'perder peso') selected @endif>Perder peso</option> 
                <option value="definicion" @if($rutina[0]->objetivo == 'definicion') selected @endif>Definición</option>      
                <option value="ganar musculo" @if($rutina[0]->objetivo == 'ganar musculo') selected @endif>Ganar músculo</option>       
            </select>

            <br>
            <br>
            <label for="link">Link video:</label><br>
            <input type="text" name="link" placeholder="https://www.youtube.com/" value="{{$link}}">

            <br>
            <div>
                <label for="dietaActual">Dieta actual</label>
                <br>
                @if($rutina[0]->pdfDieta !== null)
                    <embed src="{{ asset( $rutina[0]->pdfDieta ) }}" type="application/pdf" width="50%" height="400px" />
                @else
                    <p>No hay ninguna dieta descrita</p>
                @endif
            </div>
            <label for="diet">Cambiar dieta (opcional):</label> <br>
            <input type="file" name="diet">
            <label for="sinDieta">Eliminar dieta</label>
            <input type="checkbox" name="sinDieta" value="sinDieta">
            <br>
            <br>
            <input type="submit" value="Guardar">
        </form>
    </div>
</div>
@endsection

@section('script_tablas')
<script type="text/javascript">
$(document).ready(function() {
    // Add exercise
    var contExercise = <?php echo count($ejercicios)?>;
    var totalExercises = <?php echo $totalEjercicios?>;
    $("#addExercise").click(function() {
        contExercise++;
        totalExercises++;
        var htmlExercise = `<tr>
                            <td style="display:none;">
                                <input type="hidden" name="idEjercicioActuales[]" value=`+ totalExercises + `>
                            </td>
                            <td>
                                <input type="text" name="nameExercise[]" id="nameExercise">
                            </td>
                            <td>
                                <input type="text" name="descripcionEjercicio[]" id="descripcionEjercicio">
                            </td>
                            <td>
                                <input type="text" name="repeticiones[]" id="repeticiones">
                            </td>
                            <td>
                                
                            </td>
                            <td>
                                <input type="file" name="foto[]" id="foto">
                            </td>
                            <td>
                                <input type="text" name="dias[]" id="dias">
                            </td>
                            <td>
                                <input type="button" id="removeExercise" value="Borrar">
                            </td>
                        </tr>  `;
        $("#tableExercises").append(htmlExercise);
    });

    // Remove exercise
    $("#tableExercises").on('click', '#removeExercise', function() {
        $(this).closest('tr').remove();
        contExercise--;
    });

    
});
</script>
@stop