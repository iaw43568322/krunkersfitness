@extends('layouts.entrenador')
@section('title', 'Perfil entrenador - informafit')

@section('content')
<div class="container text-light mt-5">
    <h1 class="text-light">Mi perfil</h1>
    <div>
        <div class="mt-3" style="border: 2px solid #ccc; height: 200px; background-image: url({{ asset('./assets/img/banner/offer.png') }}); background-size: cover">
        </div>
        <p class="text-center" style="margin-top: -20px;"><img class="rounded-circle" width="125px" src="{{ $data[0]->fotoUsuario }}" alt="foto_entrenador"></p>
    </div>
    <h2 class="text-center text-light mt-3">{{ $data[0]->nombre }} {{ $data[0]->apellidos }}</h2>
    <div class="row mb-3">
        <div class="col-lg-6 col-sm-12">
            <h3 class="text-light">Sobre mi:</h3>
            <p class="text-light">{{ $data[0]->descripcion }}</p>
        </div>
        <div class="col-lg-6 col-sm-12 mb-3">
            <h3 class="text-light">Datos personales</h3>
            <p>Correo: {{ $data[0]->email }}</p>
            <p>Número de teléfono: {{ $data[0]->telefono }} </p>
            <p><i class="fa fa-facebook"></i> <i class="fa fa-instagram"></i> <i class="fa fa-google"></i></p>
        </div>
        <div class="d-flex justify-content-center mt-3">
            <a href="{{ url('/entrenador/ajustes') }}" class="boxed-btn3">Actualizar perfil</a>
            <a href="{{ url('/entrenador/darseBaja') }}" class="boxed-btn3 ml-3">Darse de baja</a>
        </div>
    </div>
</div>
@endsection