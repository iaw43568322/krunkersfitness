@extends('layouts.general')
@section('title', 'Registro Entrenador - Informafit')

@section('content')
<div class="container text-light mt-5">
    <h1 class="text-light">Registro Entrenador</h1>

    @if(app('request')->has('success'))
        @if(app('request')->input('success'))
        <div class="alert alert-success">
            Usuario registrado correctamente
        </div>
        @endif
    @endif
    @if (session('error'))
        <div class="alert alert-danger">{{ session('error') }}</div>
    @endif
    <form method="POST" action={{route('usuarios.addEntrenador')}} enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-lg-6 col-sm-12">
                @error('nombre')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Nombre</span>
                    </div>
                    <input name="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" placeholder="Juan" value="{{ old('nombre') }}" aria-label="Nombre" aria-describedby="basic-addon1">
                </div>

                @error('apellidos')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Apellidos</span>
                    </div>
                    <input name="apellidos" type="text" class="form-control @error('apellidos') is-invalid @enderror" placeholder="Lopez Jimenez" value="{{ old('apellidos') }}" aria-label="Apellidos" aria-describedby="basic-addon1">
                </div>

                @error('dni')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">DNI</span>
                    </div>
                    <input name="dni" type="text" class="form-control @error('dni') is-invalid @enderror" placeholder="12345678Z" value="{{ old('dni') }}" aria-label="dni" aria-describedby="basic-addon1">
                </div>

                @error('telefono')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Telefono mobil</span>
                    </div>
                    <input name="telefono" type="text" class="form-control @error('telefono') is-invalid @enderror" placeholder="123456789" value="{{ old('telefono') }}" aria-label="telefono" aria-describedby="basic-addon1">
                </div>
                
                @error('email')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <label for="email" class="text-light">Correo Electronico</label>
                <div class="input-group mb-3">
                    <input name="email" id="email" type="text" class="form-control @error('email') is-invalid @enderror" placeholder="juan.lopez@ejemplo.com" value="{{ old('email') }}">
                </div>

                <label for="fotoUsuario" class="text-light">Foto perfil: </label><br>
                <input type="file" name="fotoUsuario">
                <br><br>

                @error('password')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <label for="password" class="text-light">Contraseña</label>
                <div class="input-group mb-3">
                    <input name="password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" aria-label="contraseña" aria-describedby="basic-addon2">
                </div>

                @error('passwordAgain')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <label for="passwordAgain" class="text-light">Repetir Contraseña</label>
                <div class="input-group mb-3">
                    <input name="passwordAgain" id="passwordAgain" type="password" class="form-control @error('passwordAgain') is-invalid @enderror" aria-label="contraseñaAgain" aria-describedby="basic-addon2">
                </div>
            </div>
            <div class="col-lg-6 col-sm-12" style="background-image: url('./assets/img/fondoRayadoRojo.png'); background-size: cover; background-position: center;">
                <h3 class="text-light">Ventajas de ser Entrenador</h3>
                <p class="text-light" style="font-weight: bold">Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro magnam quaerat, accusamus assumenda temporibus corrupti voluptatibus commodi eveniet reprehenderit facilis voluptas corporis, odio aspernatur officiis asperiores, fugit deserunt! Est, quo unde alias sunt fugit hic soluta dolorem quam officia iure modi optio eius accusamus natus exercitationem dolor pariatur eaque quas.Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro magnam quaerat, accusamus assumenda temporibus corrupti voluptatibus commodi eveniet reprehenderit facilis voluptas corporis, odio aspernatur officiis asperiores, fugit deserunt! Est, quo unde alias sunt fugit hic soluta dolorem quam officia iure modi optio eius accusamus natus exercitationem dolor pariatur eaque quas.Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
            <br><br>
                @error('descripcion')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <div class="input-group">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Hablanos sobre ti</span>
                    </div>
                    <textarea name="descripcion" class="form-control @error('descripcion') is-invalid @enderror" aria-label="With textarea"></textarea>
                </div>
                <br>
                <input type="hidden" name="tipo" value="entrenador">
                <input type="submit" class="genric-btn primary circle" value="Registrarse">
            </div>
        </form>
    </div>
    <br>
    <p>¿Deseas registrarte como cliente? Pulsa <a class="text-light" href="{{ url('/registroCliente') }}">aqui</a></p>
</div>
@endsection