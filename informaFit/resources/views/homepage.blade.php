@extends('layouts.general')
@section('title', 'Pagina principal - informafit')

@section('content')
<!-- slider_area_start -->
<div class="slider_area mt-sm-3">
    <div class="slider_active owl-carousel">
        <div class="single_slider  d-flex align-items-center slider_bg_1 overlay">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-12">
                        <div class="slider_text text-center">
                            <h3>Bienvenido a informaFit</h3>

                            <p>Buscando tu mejor versión</p>
                            <a href="#unete" class="boxed-btn3">Únete</a>
                            <a href="{{ url('/login') }}" class="boxed-btn3 ml-3">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single_slider  d-flex align-items-center slider_bg_2 overlay">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-12">
                        <div class="slider_text text-center">
                            <h3>Las mejores rutinas de entrenamiento</h3>
                            <p>Sin importar tu nivel de entrenamiento</p>
                            <a href="#unete" class="boxed-btn3">Únete</a>
                            <a href="{{ url('/login') }}" class="boxed-btn3 ml-3">Login</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="single_slider  d-flex align-items-center slider_bg_3 overlay">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="col-xl-12">
                        <div class="slider_text text-center">
                            <h3>Tu pones el objetivo</h3>
                            <p>Nosotros te ayudamos a conseguir el cuerpo de tus sueños</p>
                            <a href="#unete" class="boxed-btn3">Únete</a>
                            <a href="{{ url('/login') }}" class="boxed-btn3 ml-3">Login</a>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->
<!-- Users start -->
<div class="container">
    <div class="row d-flex justify-content-center mt-5" id="unete">
        <br>
        <div style="background-color: black;" class="col-lg-5 ml-2 mt-3 col-md-12 col-sm-12 rounded p-3">
            <p style="font-size: 60px; text-align: center; margin-top: 20px; color: white;"><i class="fas fa-dumbbell"></i></p>
            <h2 class="text-center mt-3 text-white">Entrenador</h2>
            <h4 class="text-white mt-4">¿Que puedes hacer?</h4>
            <p class="text-white text-justify mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus eligendi, cum non placeat voluptatum tempore distinctio illum quam culpa? Iusto expedita amet veritatis a? Porro eius, consequatur suscipit quia nam, quo amet aliquid error est nihil deleniti maiores aut expedita! Dignissimos</p>
            <p class="text-center"><a href="{{ url('registroEntrenador') }}" class="boxed-btn3"">Registro Entrenador</a></p>
        </div>
        <div style="background-color: black;" class=" col-lg-5 ml-2 mt-3 col-md-12 col-sm-12 rounded p-3">
            <p style="font-size: 60px; text-align: center; margin-top: 20px; color: white;"><i class="fas fa-walking"></i></p>
            <h2 class="text-center mt-3 text-white">Cliente</h2>
            <h4 class="text-white mt-4">¿Que puedes hacer?</h4>
            <p class="text-white text-justify mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus eligendi, cum non placeat voluptatum tempore distinctio illum quam culpa? Iusto expedita amet veritatis a? Porro eius, consequatur suscipit quia nam, quo amet aliquid error est nihil deleniti maiores aut expedita! Dignissimos</p>
            <p class="text-center"><a href="{{ url('registroCliente') }}" class="boxed-btn3">Registro Cliente</a></p>
        </div>
    </div>
</div>
<!-- Users end -->
<!-- introduction start  -->
<div class="container" style="background: black;">
    <div class="row mt-5">
        <div class="col-lg-6 mt-5">
            <h2 class="text-light">¿Que es informafit?</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus eligendi, cum non placeat voluptatum tempore distinctio illum quam culpa? Iusto expedita amet veritatis a? Porro eius, consequatur suscipit quia nam, quo amet aliquid error est nihil deleniti maiores aut expedita! Dignissimos, ducimus rerum magnam id amet doloremque eius animi quis?</p>
        </div>
        <div class="col-lg-6">
            <p class="text-center mt-4"><img src="./assets/img/gallery/2.png" width="400px" alt=""></p>
        </div>
    </div>
</div>
<!-- introduction end  -->

<!-- features_area_start  -->

<div class="container my-5">
    <div class="row">
        <div class="col-lg-12">
            <div class="section_title text-center my-5">
                <h3>TÚ ELIGES UN OBJETIVO Y NOSOTROS TE DAMOS</h3>
                <h3>TODO LO NECESARIO PARA CONSEGUIRLO</h3>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-4 col-md-12 mt-3 text-center">
            <p class="text-light"><i class="fas fa-dumbbell display-4 text-light"></i></p>
            <h3 class="text-light mt-3">Plan de entrenamiento</h3>
            <p class="text-light">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam ab, veniam accusantium itaque repudiandae sit velit rerum, consectetur hic iste tenetur nihil blanditiis atque. Omnis deleniti rerum quia vel nesciunt</p>
        </div>

        <div class="col-lg-4 col-md-12 mt-3 text-center">
            <p class="text-light"><i class="fas fa-carrot display-4 text-light"></i></p>
            <h3 class="text-light mt-3">Nutrición</h3>
            <p class="text-light">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam ab, veniam accusantium itaque repudiandae sit velit rerum, consectetur hic iste tenetur nihil blanditiis atque. Omnis deleniti rerum quia vel nesciunt</p>
        </div>

        <div class="col-lg-4 col-md-12 mt-3 text-center">
            <p class="text-light"><i class="fas fa-running display-4 text-light"></i></p>
            <h3 class="text-light mt-3">Cualquier objetivo</h3>
            <p class="text-light">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam ab, veniam accusantium itaque repudiandae sit velit rerum, consectetur hic iste tenetur nihil blanditiis atque. Omnis deleniti rerum quia vel nesciunt</p>
        </div>

    </div>
    
</div>

<!-- features_area_start end  -->

<!-- necesitas una rutina start  -->

<div class="container" style="margin-top: 20px;">
    <div class="section_title text-center">
        <h3 class="">¿NECESITAT UNA RUTINA?</h3>
    </div>
    <p class="text-center mt-5"><a href="/inicioGuest" class="boxed-btn3">Empieza a buscar</a></p>
</div>

<!-- necesitas una rutina end -->

<!-- opinions start -->
<div class="container my-5">
    <div class="row">
        <div class="col-lg-12">
            <div class="section_title text-center my-5">
                <h3>NUESTROS CLIENTES</h3>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-12 mt-3 text-center">
            <img src="./assets/img/comment/comment_1.png" width="100px" alt="">
            <h3 class="text-light mt-3">Efectivo...</h3>
            <p class="text-light">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam ab, veniam accusantium itaque repudiandae sit velit rerum, consectetur hic iste tenetur nihil blanditiis atque. Omnis deleniti rerum quia vel nesciunt</p>
        </div>

        <div class="col-lg-4 col-md-12 mt-3 text-center">
            <img src="./assets/img/comment/comment_2.png" width="100px" alt="">
            <h3 class="text-light mt-3">Una mejor vida...</h3>
            <p class="text-light">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam ab, veniam accusantium itaque repudiandae sit velit rerum, consectetur hic iste tenetur nihil blanditiis atque. Omnis deleniti rerum quia vel nesciunt</p>
        </div>

        <div class="col-lg-4 col-md-12 mt-3 text-center">
            <img src="./assets/img/comment/comment_3.png" width="100px" alt="">
            <h3 class="text-light mt-3">Muy contento...</h3>
            <p class="text-light">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ullam ab, veniam accusantium itaque repudiandae sit velit rerum, consectetur hic iste tenetur nihil blanditiis atque. Omnis deleniti rerum quia vel nesciunt</p>
        </div>

    </div>
</div>

<!-- opinions end-->



<!-- team_area_start  -->
<div class="team_area team_bg_1 overlay2">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="section_title text-center mb-73">
                    <h3>Nuestros entrenadores</h3>
                    <p>There are many variations of passages of lorem Ipsum available, but the majority <br> have suffered alteration.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single_team">
                    <div class="team_thumb">
                        <img src="./assets/img/team/1.png" alt="">
                        <div class="team_hover">
                            <div class="hover_inner text-center">
                                <ul>
                                    <li><a href="#"> <i class="fa fa-facebook"></i> </a></li>
                                    <li><a href="#"> <i class="fa fa-twitter"></i> </a></li>
                                    <li><a href="#"> <i class="fa fa-instagram"></i> </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="team_title text-center">
                        <h3>Jessica Mino</h3>
                        <p>Entrenadora</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_team">
                    <div class="team_thumb">
                        <img src="./assets/img/team/2.png" alt="">
                        <div class="team_hover">
                            <div class="hover_inner text-center">
                                <ul>
                                    <li><a href="#"> <i class="fa fa-facebook"></i> </a></li>
                                    <li><a href="#"> <i class="fa fa-twitter"></i> </a></li>
                                    <li><a href="#"> <i class="fa fa-instagram"></i> </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="team_title text-center">
                        <h3>Amit Khan</h3>
                        <p>Entrenador</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_team">
                    <div class="team_thumb">
                        <img src="./assets/img/team/3.png" alt="">
                        <div class="team_hover">
                            <div class="hover_inner text-center">
                                <ul>
                                    <li><a href="#"> <i class="fa fa-facebook"></i> </a></li>
                                    <li><a href="#"> <i class="fa fa-twitter"></i> </a></li>
                                    <li><a href="#"> <i class="fa fa-instagram"></i> </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="team_title text-center">
                        <h3>Paulo Rolac</h3>
                        <p>Entrenador</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- team_area_end  -->
@endsection