<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title', 'Titulo')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <!-- <link rel="manifest" href="site.webmanifest"> -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('./assets/img/informafit_logo.png')}}">
    <!-- Place favicon.ico in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('./assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('./assets/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('./assets/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('./assets/css/font-awesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('./assets/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('./assets/css/gijgo.css')}}">
    <link rel="stylesheet" href="{{asset('./assets/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('./assets/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('./assets/css/slicknav.css')}}">

    <link rel="stylesheet" href="{{ asset('./assets/css/style.css')}}">
    <!-- <link rel="stylesheet" href="css/responsive.css"> -->

    <!--Font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/v4-shims.css">

    <!-- JQUERY -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <style>
        .video-responsive {
            position: relative;
            padding-bottom: 56.25%; /* 16/9 ratio */
            padding-top: 30px; /* IE6 workaround*/
            height: 0;
            overflow: hidden;
        }

        .video-responsive iframe,
        .video-responsive object,
        .video-responsive embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }
    </style>
</head>

<body style="background: black; color: white;">
    <!--[if lte IE 9]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p>
        <![endif]-->

    <div class="top-bar d-none d-md-block" style="margin-top: 7%;">
        <div class="container-fluid">
            <div class="row">

            </div>
        </div>
    </div>
    <!-- header-start -->
    <header>
        <div class="header-area ">
            <div id="sticky-header" class="main-header-area" style="background: black;">
                <div class="container-fluid ">
                    <div class="header_bottom_border">
                        <div class="row align-items-center">
                            <div class="col-xl-3 col-lg-2">
                                <div class="logo">
                                    <a href="{{ url('/entrenador/misRutinas') }}">
                                        <img src="{{ asset('./assets/img/informafit_logo.png')}}" width="180px" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-7">
                                <div class="main-menu  d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">
                                            <li><a href="{{ url('/entrenador/misRutinas') }}" style="font-size: 18px;">Mis rutinas</a></li>
                                            <li><a href="{{ url('/entrenador/miPerfil') }}" style="font-size: 18px;">Mi perfil</a></li>
                                            <li><a href="{{ url('/entrenador/ajustes') }}" style="font-size: 18px;">Ajustes</a></li>
                                            <li><a class="d-lg-none d-sm-block" href="{{ url('/logout') }}" style="font-size: 18px;">Cerrar sesión</a></li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 d-none d-lg-block">
                                <div class="Appointment dropdown">
                                    <p class="text-light mr-3">{{ (app('request')->session()->get('usuario')['nombre']) }} {{ (app('request')->session()->get('usuario')['apellidos']) }} </p>
                                    
                                        <img class="dropdown-toggle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" src="{{ asset((app('request')->session()->get('usuario')['foto'])) }}" width="40px" alt="foto_perfil_entrenador">
                                    
                                    <div class="dropdown-menu dropdown-menu-right mt-2" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{ url('/logout') }}">Cerrar sesión</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="mobile_menu d-block d-lg-none"></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </header>
    <!-- header-end -->












    <!-- big_offer_area start  -->
    <div class="">
        @yield('content')
    </div>
    <!-- big_offer_area end  -->




    <!-- footer_start  -->
    <footer class="footer">
        <div class="footer_top">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-lg-3 ">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="#">
                                    <img src="{{ asset('./assets/img/footer_logo.png')}}" alt="">
                                </a>
                            </div>
                            <p>5th flora, 700/D kings road, green <br> lane New York-1782 <br>
                                <a href="#">+10 367 826 2567</a> <br>
                                <a href="#">contact@carpenter.com</a>
                            </p>
                            <p>



                            </p>
                            <div class="socail_links">
                                <ul>
                                    <li>
                                        <a href="#">
                                            <i class="ti-facebook"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="ti-twitter-alt"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-instagram"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-pinterest"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-youtube-play"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-lg-4 offset-xl-1">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Useful Links
                            </h3>
                            <ul class="links">
                                <li><a href="#">Pricing</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#"> Gallery</a></li>
                                <li><a href="#"> Contact</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6 col-lg-4">
                        <div class="footer_widget">
                            <h3 class="footer_title">
                                Subscribe
                            </h3>
                            <form action="#" class="newsletter_form">
                                <input type="text" placeholder="Enter your mail">
                                <button type="submit">Subscribe</button>
                            </form>
                            <p class="newsletter_text">Esteem spirit temper too say adieus who direct esteem esteems
                                luckily.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                                document.write(new Date().getFullYear());
                            </script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer_end  -->

    @yield('script_tablas')


    <!-- JS here -->
    <script src="{{ asset('./assets/js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{ asset('./assets/js/vendor/jquery-1.12.4.min.js')}}"></script>
    <script src="{{ asset('./assets/js/popper.min.js')}}"></script>
    <script src="{{ asset('./assets/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('./assets/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('./assets/js/isotope.pkgd.min.js')}}"></script>
    <script src="{{ asset('./assets/js/ajax-form.js')}}"></script>
    <script src="{{ asset('./assets/js/waypoints.min.js')}}"></script>
    <script src="{{ asset('./assets/js/jquery.counterup.min.js')}}"></script>
    <script src="{{ asset('./assets/js/imagesloaded.pkgd.min.js')}}"></script>
    <script src="{{ asset('./assets/js/scrollIt.js')}}"></script>
    <script src="{{ asset('./assets/js/jquery.scrollUp.min.js')}}"></script>
    <script src="{{ asset('./assets/js/wow.min.js')}}"></script>
    <script src="{{ asset('./assets/js/gijgo.min.js')}}"></script>
    <script src="{{ asset('./assets/js/nice-select.min.js')}}"></script>
    <script src="{{ asset('./assets/js/jquery.slicknav.min.js')}}"></script>
    <script src="{{ asset('./assets/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{ asset('./assets/js/plugins.js')}}"></script>



    <!--contact js-->
    <script src="{{ asset('./assets/js/contact.js')}}"></script>
    <script src="{{ asset('./assets/js/jquery.ajaxchimp.min.js')}}"></script>
    <script src="{{ asset('./assets/js/jquery.form.js')}}"></script>
    <script src="{{ asset('./assets/js/jquery.validate.min.js')}}"></script>
    <script src="{{ asset('./assets/js/mail-script.js')}}"></script>


    <script src="{{ asset('./assets/js/main.js')}}"></script>

</body>

</html>
