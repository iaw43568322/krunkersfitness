<button id="btnFilters" type="button" class="btn btn-sm btn-primary d-sm-block d-md-block d-lg-none mb-2" style="margin: 0 auto;">
        Mostrar filtros
</button>
<!-- Modal -->
<div id="modal" class="modal" style="background-color: black;">
    <div class="contModal" style="overflow-y: scroll; ">
        <span id="close" class="close text-light">&times;</span>
        <div class="ml-5 mt-5">
        <!-- Form palabra clave -->
        <form id="filterKeywordMini" method="post" action="/palabraClaveRutina">
            {{ csrf_field() }}
            <!-- Palabras clave-->
            <h4 class="text-light">Palabra clave</h4>
            <input type="text" placeholder="Buscar rutina" name="keyword" value="{{ old('keyword')}}" id="form-keyword">
            <br><br>
            <input type="submit" value="Buscar">
        </form>
        <br>

        <!-- Form filtros -->
        <form id="filtrosMini" method="post" action="/filterRutina">
            {{ csrf_field() }}
            <div>
            <h5 class="text-light">Dificultad</h5>
            <input type="checkbox" name="dificultad[]" value="facil">
            <label for="dificultad">Fácil</label>
            <input type="checkbox" name="dificultad[]" value="intermedio">
            <label for="dificultad">Intermedio</label>
            <input type="checkbox" name="dificultad[]" value="dificil">
            <label for="dificultad">Difícil</label><br>
            </div>
            <br>
            <h5 class="text-light">Duración</h5>
            <label for="duracion" class="text-dark">Mes</label>
            <br>
            <select name="duracion" id="duracion" class="text-dark" style="padding-right: 20px;">
                @for ($i = 0; $i <= 12; $i++)
                    <option>{{ $i }}</option>
                @endfor
            </select>
            <br><br>

            <h5 class="text-light">Zona de Trabajo</h5>

            <input type="checkbox" name="zonaTrabajo[]" value="Exterior">
            <label for="zonaTrabajo">Exterior</label>
            <input type="checkbox" name="zonaTrabajo[]" value="Gimnasio">
            <label for="zonaTrabajo">Gimnasio</label>
            <input type="checkbox" name="zonaTrabajo[]" value="Casa">
            <label for="zonaTrabajo">Casa</label><br>
            <h5 class="text-white">Objetivo</h5>

            <input type="checkbox" name="objetivo[]" value="perder peso">
            <label for="objetivo">Perder peso</label>
            <input type="checkbox" name="objetivo[]" value="definicion">
            <label for="objetivo">Definición</label>
            <input type="checkbox" name="objetivo[]" value="ganar musculo">
            <label for="objetivo">Ganar Músculo</label><br>
            <input type="submit" value="Buscar">
        </form>
        </div>
    </div>
</div>




<div id="filtros2" class="col-lg-3 order-lg-2 order-sm-1 mr-1 rounded p-4 d-sm-none d-lg-block collapse" style="height: 800px; background-color: black; border: 6px solid white; border-radius: 10px 20px 20px 10px;">
    <!-- Form palabra clave -->
    <form id="filterKeyword" method="post" action="/palabraClaveRutina">
        {{ csrf_field() }}
        <!-- Palabras clave-->
        <h4 class="text-white">Palabra clave</h4>
        <input type="text" placeholder="Buscar rutina" name="keyword" value="{{ old('keyword')}}" id="form-keyword" class="form-control me-2">
        <br><br>
        <input type="submit" value="Buscar">
    </form>
    <br>

    <!-- Form filtros -->
    <form id="filtros" method="post" action="/filterRutina">
        {{ csrf_field() }}
        <h5 class="text-white">Dificultad</h5>
        <!-- <div class="form-check">
            <input type="radio" class="form-check-input" name="dificultad" id="facil" value="facil">
            <label for="facil" class="form-check-label">Fácil</label>
        </div>

        <div class="form-check">
            <input type="radio" class="form-check-input" name="dificultad" id="medio" value="intermedio">
            <label for="medio" class="form-check-label">Medio</label>
        </div>

        <div class="form-check">
            <input type="radio" class="form-check-input" name="dificultad" id="dificil" value="dificil">
            <label for="dificil" class="form-check-label">Dificil</label>
        </div> -->
        <input type="checkbox" name="dificultad[]" value="facil">
        <label for="dificultad">Fácil</label><br>
        <input type="checkbox" name="dificultad[]" value="intermedio">
        <label for="dificultad">Intermedio</label><br>
        <input type="checkbox" name="dificultad[]" value="dificil">
        <label for="dificultad">Difícil</label><br>

        <br>
        <h5 class="text-white">Duración</h5>
        <label for="duracion" class="text-white">Mes</label>
        <br>
        <select name="duracion" id="duracion" class="text-dark" style="padding-right: 20px;">
            @for ($i = 0; $i <= 12; $i++)
                <option>{{ $i }}</option>
            @endfor
        </select>
        <br><br>
        
        <h5 class="text-white">Zona de Trabajo</h5>
        <!-- <select class="form-select form-select-sm text-dark" id="zonaTrabajo" name="zonaTrabajo">
            <option value="" selected disabled>Selecciona una opcion</option>
            <option value="Exterior">Exterior</option>
            <option value="Gimnasio">Gimnasio</option>
            <option value="Casa">Casa</option>
        </select> -->
        <input type="checkbox" name="zonaTrabajo[]" value="Exterior">
        <label for="zonaTrabajo">Exterior</label><br>
        <input type="checkbox" name="zonaTrabajo[]" value="Gimnasio">
        <label for="zonaTrabajo">Gimnasio</label><br>
        <input type="checkbox" name="zonaTrabajo[]" value="Casa">
        <label for="zonaTrabajo">Casa</label><br>
        <h5 class="text-white">Objetivo</h5>
        <!-- <select class="form-select form-select-sm text-dark" id="objetivo" name="objetivo">
            <option value="" selected disabled>Selecciona una opcion</option>
            <option value="perderPeso">Perder Peso</option>
            <option value="definicion">Definición</option>
            <option value="ganarMusculo">Ganar Musculo</option>
        </select> -->
        <input type="checkbox" name="objetivo[]" value="perder peso">
        <label for="objetivo">Perder peso</label><br>
        <input type="checkbox" name="objetivo[]" value="definicion">
        <label for="objetivo">Definición</label><br>
        <input type="checkbox" name="objetivo[]" value="ganar musculo">
        <label for="objetivo">Ganar Músculo</label><br>
        <input type="submit" value="Buscar">
    </form>
</div>

<script>
    // MADE WITH JQUERY
    $('#filterKeyword').submit(function(e) {
        e.preventDefault();
        var data = $('#filterKeyword').serialize();
        axios.post('/palabraClaveRutina', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )
    // MADE WITH JQUERY
    $('#filtros').submit(function(e) {
        e.preventDefault();
        var data = $('#filtros').serialize();
        axios.post('/filterRutina', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )

    // MADE WITH JQUERY
    $('#filterKeywordMini').submit(function(e) {
        e.preventDefault();
        var data = $('#filterKeywordMini').serialize();
        axios.post('/palabraClaveRutina', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )
    // MADE WITH JQUERY
    $('#filtrosMini').submit(function(e) {
        e.preventDefault();
        var data = $('#filtrosMini').serialize();
        axios.post('/filterRutina', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )

</script>
