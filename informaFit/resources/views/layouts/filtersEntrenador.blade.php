<button id="btnFilters" type="button" class="btn btn-sm btn-primary d-sm-block d-md-block d-lg-none mb-2" style="margin: 0 auto;">
        Mostrar filtros
</button>
<!-- Modal -->
<div id="modal" class="modal" style="background-color: black;">
    <div class="contModal" style="overflow-y: scroll; ">
        <span id="close" class="close text-light" >&times;</span>
        <div class="ml-5 mt-5">
        <!-- Form palabra clave -->
        <form id="filterKeywordMini" method="post" action="/palabraClaveRutinaEntrenador">
            {{ csrf_field() }}
            <!-- Palabras clave-->
            <h4 class="text-light">Palabra clave</h4>
            <input type="text" placeholder="Buscar rutina" name="keyword" value="{{ old('keyword')}}" id="form-keyword" class="me-2">
            <br><br>
            <input type="submit" value="Buscar">
        </form>
        <br>

        <!-- Form filtros -->
        @if (app('request')->session()->get('tipoUsuario') == 'cliente')
        <form id="filtrosOrdenClienteMini" method="post" action="/ordenarSuscripciones">
            {{ csrf_field() }}
            <h4 class="text-light">Ordenar por</h4>
            <label for="orden" class="text-white">Dificultad</label>
            <input type="radio" name="orden" id="dificultad" value="dificultad">
            <br>
            <label for="orden" class="text-white">Fecha de publicación</label>
            <input type="radio" name="orden" id="fechaPublicacion" value="created_at">
            <br>
            
            <label for="orden" class="text-white">Progreso</label>
            <input type="radio" name="orden" id="progreso" value="progreso">
            <br><br>
            <input type="submit" value="Buscar">
        </form>
        @else
        <form id="filtrosOrdenMini" method="post" action="/orderFilterRutina">
            {{ csrf_field() }}
            <h4 class="text-light">Ordenar por</h4>
            <label for="orden" class="text-white">Dificultad</label>
            <input type="radio" name="orden" id="dificultad" value="dificultad">
            <br>
            <label for="orden" class="text-white">Fecha de publicación</label>
            <input type="radio" name="orden" id="fechaPublicacion" value="created_at">
            <br><br>
            <input type="submit" value="Buscar">
        </form>
        
        <br><br>
        <a href="/entrenador/nuevaRutina">
            <button class="btn btn-warning font-weight-bold text-light">Crear Rutina</button>
        </a>
        <br><br>
        <form action="eliminarRutinasEntrenador" method="POST" onsubmit="return confirm('¿Quieres eliminar todas las rutinas?');">
            {{ csrf_field() }}
            <input class="btn btn-danger" type="submit" value="Eliminar rutinas">
        </form>
        @endif
        </div>
    </div>
</div>




<div id="filtros" class="col-lg-3 order-lg-2 order-sm-1 mr-1 rounded p-4 d-sm-none d-lg-block collapse" style="height: 800px; background-color: black; border: 6px solid white; border-radius: 10px 20px 20px 10px;">
    <!-- Form palabra clave -->
    <form id="filterKeyword" method="post" action="/palabraClaveRutinaEntrenador">
        {{ csrf_field() }}
        <!-- Palabras clave-->
        <h4 class="text-light">Palabra clave</h4>
        <input type="text" placeholder="Buscar rutina" name="keyword" value="{{ old('keyword')}}" id="form-keyword" class="form-control me-2">
        <br><br>
        <input type="submit" value="Buscar">
        <!-- <button type="submit">Buscar</button> -->
    </form>
    <br>

    <!-- Form filtros -->
    @if (app('request')->session()->get('tipoUsuario') == 'cliente')
    <form id="filtrosOrdenCliente" method="post" action="/ordenarSuscripciones">
        {{ csrf_field() }}
        <h4 class="text-light">Ordenar por</h4>
        <label for="orden" class="text-white">Dificultad</label>
        <input type="radio" name="orden" id="dificultad" value="dificultad">
        <br>
        <label for="orden" class="text-white">Fecha de publicación</label>
        <input type="radio" name="orden" id="fechaPublicacion" value="created_at">
        <br>
        
        <label for="orden" class="text-white">Progreso</label>
        <input type="radio" name="orden" id="progreso" value="progreso">
        <br><br>
        <input type="submit" value="Buscar">
    </form>
    @else
    <form id="filtrosOrden" method="post" action="/orderFilterRutina">
        {{ csrf_field() }}
        <h4 class="text-light">Ordenar por</h4>
        <label for="orden" class="text-white">Dificultad</label>
        <input type="radio" name="orden" id="dificultad" value="dificultad">
        <br>
        <label for="orden" class="text-white">Fecha de publicación</label>
        <input type="radio" name="orden" id="fechaPublicacion" value="created_at">
        <br><br>
        <input type="submit" value="Buscar">
    </form>
    
    <br><br>
    <a href="/entrenador/nuevaRutina">
        <button class="btn btn-warning font-weight-bold text-light">Crear Rutina</button>
    </a>
    <br><br>
    <form action="eliminarRutinasEntrenador" method="POST" onsubmit="return confirm('¿Quieres eliminar todas las rutinas?');">
        {{ csrf_field() }}
        <input class="btn btn-danger" type="submit" value="Eliminar rutinas">
    </form>
    @endif

    <br>
    <p>Usuarios registrados: {{$countUsers}}</p>

</div>

<script>
    // // MADE WITH JQUERY
    $('#filterKeyword').submit(function(e) {
        e.preventDefault();
        var data = $('#filterKeyword').serialize();
        axios.post('/palabraClaveRutinaEntrenador', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )
    // // MADE WITH JQUERY
    // Filtros entrenador
    $('#filtrosOrden').submit(function(e) {
        e.preventDefault();
        var data = $('#filtrosOrden').serialize();
        axios.post('/orderFilterRutina', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )

    // Filtros cliente
    $('#filtrosOrdenCliente').submit(function(e) {
        e.preventDefault();
        var data = $('#filtrosOrdenCliente').serialize();
        axios.post('/ordenarSuscripciones', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )

    // // MADE WITH JQUERY
    $('#filterKeywordMini').submit(function(e) {
        e.preventDefault();
        var data = $('#filterKeywordMini').serialize();
        axios.post('/palabraClaveRutinaEntrenador', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )
    // // MADE WITH JQUERY
    // Filtros entrenador
    $('#filtrosOrdenMini').submit(function(e) {
        e.preventDefault();
        var data = $('#filtrosOrdenMini').serialize();
        axios.post('/orderFilterRutina', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )

    // Filtros cliente
    $('#filtrosOrdenClienteMini').submit(function(e) {
        e.preventDefault();
        var data = $('#filtrosOrdenClienteMini').serialize();
        axios.post('/ordenarSuscripciones', data)
            .then(response => {
                console.log(response);
                $('#rutinaList').replaceWith(response.data)
            })
        }
    )

</script>
