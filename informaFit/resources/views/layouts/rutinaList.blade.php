
    @forelse ($rutinas as $rutina)
        <a href="descripcionRutina?idRutina={{ $rutina->idRutina }}">
        <div class="border mb-5" style="background-color: white;">
            @switch($rutina->objetivo)
                @case("ganar musculo")
                    <img src="{{ asset('.././assets/img/banner/bradcam_2.png') }}" style="width: 100%;">
                    @break
                @case("definicion")
                    <img src="{{ asset('.././assets/img/banner/bradcam_1.png') }}" style="width: 100%;">
                    @break
                @case("perder peso")
                    <img src="{{ asset('.././assets/img/banner/banner2.png') }}" style="width: 100%;">
                    @break
                @default
                    <img src="{{ asset('.././assets/img/banner/big_offer.png') }}" style="width: 100%;">
            @endswitch
            <div class="p-3 pb-5">
                <h1>{{ $rutina->titulo }}</h1>
                <p style="color: #001d38;"> <span class="font-weight-bold">Entrenador:</span> {{ $rutina->usuarioNom }} {{ $rutina->apellidos }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Descripción:</span> {{ $rutina->rutinaDesc }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Dificultad:</span> {{ $rutina->dificultad }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Duración:</span> {{ $rutina->duracion }} mes/es</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Materiales:</span> {{ $rutina->materiales }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Objetivo:</span> {{ $rutina->objetivo }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Zona de trabajo:</span> {{ $rutina->zonaTrabajo }}</p>
                <a style="color: #001d38; float: right;" href="descripcionRutina?idRutina={{ $rutina->idRutina }}">Leer más...</a>
            </div>
        </div>
        </a>
        @empty
        <p>No hay ninguna rutina</p>
    @endforelse
    
