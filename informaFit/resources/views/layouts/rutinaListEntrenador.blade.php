@forelse ($rutinasEntrenador as $rutina)
    <a href="descripcionRutina?idRutina={{ $rutina->idRutina }}">
        <div class="border mb-5" style="background-color: white;">
            @switch($rutina->objetivo)
                @case("ganar musculo")
                    <img src="{{ asset('.././assets/img/banner/bradcam_2.png') }}" style="width: 100%;">
                    @break
                @case("definicion")
                    <img src="{{ asset('.././assets/img/banner/bradcam_1.png') }}" style="width: 100%;">
                    @break
                @case("perder peso")
                    <img src="{{ asset('.././assets/img/banner/banner2.png') }}" style="width: 100%;">
                    @break
                @default
                    <img src="{{ asset('.././assets/img/banner/big_offer.png') }}" style="width: 100%;">
            @endswitch
            <div class="p-3 mb-3">
                <h1>{{ $rutina->titulo }}</h1>
                <p style="color: #001d38;"> <span class="font-weight-bold">Descripción:</span> {{ $rutina->descripcion }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Dificultad:</span> {{ $rutina->dificultad }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Duración:</span> {{ $rutina->duracion }} mes/es</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Materiales:</span> {{ $rutina->materiales }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Objetivo:</span> {{ $rutina->objetivo }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Zona de trabajo:</span> {{ $rutina->zonaTrabajo }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Día creación:</span> {{ date_format($rutina->created_at,"d/m/Y") }}</p>
                <p style="color: #001d38;"> <span class="font-weight-bold">Usuarios suscritos:</span> {{ $suscripcionRutina[$loop->index] }}</p>
                <br>
                <form action="eliminarRutinaEntrenador" method="POST" onsubmit="return confirm('¿Quieres eliminar esta rutina?');">
                    {{ csrf_field() }}
                    <input type="hidden" name="idRutina" value="{{$rutina->idRutina}}">
                    <input class="btn btn-danger" type="submit" value="Eliminar">
                </form>
                <!-- <button class="btn btn-danger"><a class="text-light text-bold font-weight-bold" href="/eliminarRutina?idRutina={{$rutina->idRutina}}">Eliminar</a></button> -->
                <a style="color: #001d38; float: right;" href="descripcionRutina?idRutina={{ $rutina->idRutina }}">Leer más...</a>
            </div>
        </div>
        </a>
        @empty
        <p>No hay rutinas</p>
    @endforelse
