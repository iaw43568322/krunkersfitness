@forelse ($rutinas as $rutina)
<a href="descripcionRutina?idRutina={{ $rutina->idRutina }}">
    <div class="border mb-5" style="background-color: white;">
        @switch($rutina->objetivo)
            @case("ganar musculo")
                <img src=".././assets/img/banner/bradcam_2.png" style="width: 100%;">
                @break
            @case("definicion")
                <img src=".././assets/img/banner/bradcam_1.png" style="width: 100%;">
                @break
            @case("perder peso")
                <img src=".././assets/img/banner/banner2.png" style="width: 100%;">
                @break
            @default
                <img src=".././assets/img/banner/big_offer.png" style="width: 100%;">
        @endswitch
        <div class="p-3">
            <h1>{{ $rutina->titulo }}</h1>
            <p style="color: #001d38;"> <span class="font-weight-bold">Entrenador:</span> {{ $rutina->nombreEntrenador }} {{ $rutina->apellidosEntrenador }}</p>
            <p style="color: #001d38;"> <span class="font-weight-bold">Descripción:</span> {{ $rutina->rutinaDesc }}</p>
            <p style="color: #001d38;"> <span class="font-weight-bold">Dificultad:</span> {{ $rutina->dificultad }}</p>
            <p style="color: #001d38;"> <span class="font-weight-bold">Duración:</span> {{ $rutina->duracion }} mes/es</p>
            <p style="color: #001d38;"> <span class="font-weight-bold">Materiales:</span> {{ $rutina->materiales }}</p>
            <p style="color: #001d38;"> <span class="font-weight-bold">Objetivo:</span> {{ $rutina->objetivo }}</p>
            <p style="color: #001d38;"> <span class="font-weight-bold">Zona de trabajo:</span> {{ $rutina->zonaTrabajo }}</p>
            <p style="color: #001d38;"> <span class="font-weight-bold">Día creación:</span> {{ date_format(date_create($rutina->rutinaFecha),"d/m/Y") }}</p>
            <br>
            <p>
                <progress id="progreso" max="100" value="{{$rutina->diasCompletados * 100 / $rutina->diasTotales}}"></progress>
                {{round($rutina->diasCompletados * 100 / $rutina->diasTotales, 2)}} %
            </p>
            <br>
            <form action="eliminarSuscripcion" method="POST" onsubmit="return confirm('¿Quieres eliminar esta rutina de tus suscripciones?');">
                {{ csrf_field() }}
                <input type="hidden" name="idRutina" value="{{$rutina->idRutina}}">
                <input class="btn btn-danger" type="submit" value="Eliminar rutina">
            </form>
            <a style="color: #001d38; float: right; padding-bottom: 15px;" href="descripcionRutina?idRutina={{ $rutina->idRutina }}">Leer más...</a>
        </div>
    </div>
    </a>
@empty
    <p>No hay rutinas</p>
@endforelse