@extends('layouts.general')
@section('title', 'Iniciar sesión - informafit')

@section('content')
<div style="background-image: url('./assets/img/banner/pesas.jpg'); background-size: cover; padding: 15px;">

<!-- Form start -->
<div class="container p-5 mt-lg-3 mt-sm-5 mt-xs-5">
    <div class="row">
        <div class="col align-self-center" style="width: 80%; display: flex; align-items: center; justify-content: center;">
            <form action="/loginCheck" method="POST" class="bg-light text-dark p-5 rounded">
                {{ csrf_field() }}
                <h3>Iniciar Sesión</h3>
                @if (session('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>
                @endif
                @error('email')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <i class="fas fa-envelope"></i>
                <label for="email" class="text-dark">Correo electrónico</label>
                <input class="form-control @error('email') is-invalid @enderror" type="text" name="email" id="correoElectronico" value="">
                <br>
                @error('password')
                    <span class="text-danger">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br>
                @enderror
                <i class="fas fa-unlock-alt"></i>
                <label for="password" class="text-dark">Contraseña</label>
                <br>
                <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="contraseña" value="">
                <br>
                <input type="submit" class="btn btn-block btn-success" value="Login">
                <br>
                <p>¿No estas registrado? <a style="font-size:14px;" href="#registro">Únete</a></p>
            </form>
        </div>
    </div>
</div>
<!-- Form end -->

<!-- Users start -->
<div class="container mt-3 mb-3">
    <div class="row d-flex justify-content-center" id="registro">
        <br>
        <div style="background-color: black;" class="col-lg-5 ml-2 mt-3 col-md-12 col-sm-12 rounded border border-danger p-5">
            <p style="font-size: 60px; text-align: center; margin-top: 20px; color: white;"><i class="fas fa-dumbbell"></i></p>
            <h2 class="text-center mt-3 text-white">Entrenador</h2>
            <h4 class="text-white mt-4">¿Que puedes hacer?</h4>
            <p class="text-white mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus eligendi, cum non placeat voluptatum tempore distinctio illum quam culpa? Iusto expedita amet veritatis a? Porro eius, consequatur suscipit quia nam, quo amet aliquid error est nihil deleniti maiores aut expedita! Dignissimos, ducimus rerum magnam id amet doloremque eius animi quis?</p>
            <p class="text-center"><a href="{{ url('registroEntrenador') }}" class="boxed-btn3"">Registro Entrenador</a></p>
        </div>
        <div style="background-color: black;" class=" col-lg-5 ml-2 mt-3 col-md-12 col-sm-12 rounded border border-danger p-5">
            <p style="font-size: 60px; text-align: center; margin-top: 20px; color: white;"><i class="fas fa-walking"></i></p>
            <h2 class="text-center mt-3 text-white">Cliente</h2>
            <h4 class="text-white mt-4">¿Que puedes hacer?</h4>
            <p class="text-white mb-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus eligendi, cum non placeat voluptatum tempore distinctio illum quam culpa? Iusto expedita amet veritatis a? Porro eius, consequatur suscipit quia nam, quo amet aliquid error est nihil deleniti maiores aut expedita! Dignissimos, ducimus rerum magnam id amet doloremque eius animi quis?</p>
            <p class="text-center"><a href="{{ url('registroCliente') }}" class="boxed-btn3">Registro Cliente</a></p>
        </div>
    </div>
</div>
<!-- Users end -->


</div>
@endsection