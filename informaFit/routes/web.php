<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\RutinaController;
use App\Http\Controllers\UsuarioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('homepage');
});

/***************
 *
 * Rutas GENERALES
 *
 ****************/

// Ruta para mostrar la vista login
Route::get('/login', [AdminController::class, 'showLogin']);
// Ruta para comprobar que el usuario existe
Route::post('/loginCheck', [AdminController::class, 'loginUser']);
// Ruta para mostrar todas las rutinas a guest
Route::get('/inicioGuest', [RutinaController::class, 'mostrarRutinas']);

/***************
 *
 * Rutas Cliente
 *
 ****************/
Route::group(['prefix' => 'cliente', 'middleware' => ['checkCliente']], function() {
    // Perfil cliente
    Route::get('miPerfil', [UsuarioController::class, 'dataUsuarioCliente']);
    // Inicio Cliente
    Route::get('inicioCliente', [RutinaController::class, 'mostrarRutinas']);
    // Ajustes Cliente
    Route::get('ajustes', [UsuarioController::class, 'showAjustesCliente']);
    // Darse de baja
    Route::get('darseBaja', function() {
        return view('cliente.darseBajaCliente');
    });
    // Mis rutinas
    Route::get('misRutinas', [UsuarioController::class, 'misRutinasCliente']);
    // Mis rutinas con filtro de ordenar
    Route::post('misRutinasOrdenar', [RutinaController::class, 'ordenarRutinaFiltros']);
    // Mis rutinas con palabra clave
    Route::post('misRutinasPalabraClave', [RutinaController::class, 'misRutinasClientePalabra']);
    // Eliminar suscripcion
    Route::post('/eliminarSuscripcion', [RutinaController::class, 'eliminarSuscripcion']);
    // Eliminar suscripciones
    Route::post('/eliminarSuscripciones', [RutinaController::class, 'eliminarSuscripciones']);
    // Descripción rutina
    Route::get('descripcionRutina', [RutinaController::class, 'descripcionRutina']);
    //Suscripcion Rutina
    Route::post('suscripcionRutina', [UsuarioController::class, 'suscripcionRutina']);
    // Añadir 1 dia a diasCompletados
    Route::post('añadirDiaCompletado', [UsuarioController::class, 'añadirDiaCompletado']);
});

//Inicio Cliente con Filtros
Route::post('filterRutina', [RutinaController::class, 'mostrarRutinaFiltros']);

//Inicio Cliente con Palabra Clave
Route::post('palabraClaveRutina', [RutinaController::class, 'mostrarPalabraClave']);

// Filtro ordenar suscripciones
Route::post('ordenarSuscripciones', [RutinaController::class, 'ordenarSuscripciones']);

// Ajustes Cliente
// Route::get('cliente/ajustes', function() {
//     return view('cliente.ajustesCliente');
// });
// // Darse de baja
// Route::get('cliente/darseBaja', function() {
//     return view('cliente.darseBajaCliente');
// });
// // Mis rutinas
// Route::get('cliente/misRutinas', function() {
//     return view('cliente.misRutinasCliente');
// })->middleware("checkCliente");
// Registro Usuario
Route::get('registroCliente', function() {
    return view('cliente.registroCliente');
});

// Descripción rutina
/* Route::get('cliente/descripcionRutina', function() {
    return view('cliente.descripcionRutina');
}); */

//Suscripcion Rutina
Route::get('cliente/suscripcionRutina', function() {
    return view('cliente.suscripcionRutina');
});
//Suscripcion Rutina
Route::get('cliente/suscripcionRutina', function() {
    return view('cliente.suscripcionRutina');
});
// // Descripción rutina
// Route::get('cliente/descripcionRutina', function() {
//     return view('cliente.descripcionRutina');
// });
// //Suscripcion Rutina
// Route::get('cliente/suscripcionRutina', function() {
//     return view('cliente.suscripcionRutina');
// });
// //Suscripcion Rutina
// Route::get('cliente/suscripcionRutina', function() {
//     return view('cliente.suscripcionRutina');
// });

/***************
 *
 * Rutas Entrenador
 *
 ****************/
Route::group(['prefix' => 'entrenador', 'middleware' => ['checkEntrenador']], function() {
    // Mis rutinas
    Route::get('misRutinas', [AdminController::class, 'showInicioEntrenador']);
    // Crear rutina
    Route::get('nuevaRutina', function() {
        return view('entrenador.crearRutina');
    });
    // Modificar rutina
    Route::get('showModificarRutina', [RutinaController::class, 'showModificarRutina']);
    Route::post('modificarRutina', [RutinaController::class, 'modificarRutina']);
    // Rutina creada por entrenador
    // Route::get('descripcionRutina', function() {
    //     return view('entrenador.descripcionRutina');
    // });
    // Eliminar rutina
    Route::post('/eliminarRutinaEntrenador', [RutinaController::class, 'eliminarRutinaEntrenador']);
    // Eliminar rutinas
    Route::post('/eliminarRutinasEntrenador', [RutinaController::class, 'eliminarRutinasEntrenador']);
    // Descripción rutina
    Route::get('descripcionRutina', [RutinaController::class, 'descripcionRutina']);
    // Mi perfil
    Route::get('miPerfil', [UsuarioController::class, 'dataUsuarioEntrenador']);
    // Ajustes entrenador
    Route::get('ajustes', [UsuarioController::class, 'showAjustesEntrenador']);
    // Darse de baja
    Route::get('darseBaja', function() {
        return view('entrenador.darseBajaEntrenador');
    });
    // Crear rutina
    Route::post('crearRutina', [RutinaController::class, 'crearRutina']);


    // Rutinas entrenador
});
//Filtro Mis Rutinas Entrenador
Route::post('orderFilterRutina', [RutinaController::class, 'ordenarRutinaFiltros']);

//Mis Rutinas Con Palabra Clave
Route::post('palabraClaveRutinaEntrenador', [RutinaController::class, 'mostrarPalabraClaveEntrenador']);


/* ------ Rutas general -------- */
// Registro Usuario
Route::get('/registroCliente', [AdminController::class, 'mostrarRegistroCliente']);
Route::post("/registrarCliente", [AdminController::class, 'crearUsuarioCliente'])-> name('usuarios.addCliente');
//Registro entrenador
Route::get('/registroEntrenador', [AdminController::class, 'mostrarRegistroEntrenador']);
Route::post('/registrarEntrenador', [AdminController::class, 'crearUsuarioEntrenador'])-> name('usuarios.addEntrenador');
// Modificar contraseña
Route::post('/modificarPassword', [UsuarioController::class, 'modificarPassword']);
// Darse de baja
Route::post('/bajaUsuario', [UsuarioController::class, 'darseBaja']);
// Cerrar sesión
Route::get('/logout', [AdminController::class, 'logoutUser']);



